/**
 * Created by AranciaCheng on 2017/5/13.
 */
// var extractTextPlugin = require('extract-text-webpack-plugin');
module.exports = {
  // styleLoader: extractTextPlugin.extract({
  //   fallback:'style-loader',
  //   use:'less-loader'
  // }),
  styles: {
    "mixins": true,

    "core": true,
    "icons": true,

    "larger": true,
    "path":true
  }
};