defmodule CentralControl.Manager do
    use Ecto.Schema
    require Ecto.Query

    schema "manager" do
        field :userid, :integer
        field :clubid, :integer

        timestamps()
    end

    def changeset(msg, params \\ %{}) do
        msg
        |> Ecto.Changeset.cast(params, [:clubid, :userid])
        |> Ecto.Changeset.validate_required([:clubid, :userid])

    end
    
end