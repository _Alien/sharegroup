defmodule CentralControl.PartIn do
    use Ecto.Schema
    require Ecto.Query

    schema "partin" do
        field :userid, :integer
        field :clubid, :integer
        field :duty, :string, default: "成员"

        timestamps()
    end

    def changeset(msg, params \\ %{}) do
        msg
        |> Ecto.Changeset.cast(params, [:clubid, :userid, :duty])
        |> Ecto.Changeset.validate_required([:clubid, :userid])

    end
    
end