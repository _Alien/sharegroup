defmodule CentralControl.OwnerAdapter do
    
    alias CentralControl.Owner
    alias CentralControl.Repo

    require Ecto.Query

    @doc """
    创建对应的社团以及拥有者
    """
    def create_owner(params) do
        %Owner{} |> Owner.changeset(params) |> Repo.insert
    end
    
    @doc """
    搜索用户拥有的所有社团，初步不做任何个数限制
    """
    def clublist(userid) do
        Owner
        |> Ecto.Query.where(userid: ^userid)
        |> Repo.all
    end

    @doc """
    搜索社团对应的拥有者
    """
    def findOwner(clubid) do
        Owner
        |> Ecto.Query.where(clubid: ^clubid)
        |> Repo.one
    end

    def deleteclub(clubid) do
        Owner
        |> Ecto.Query.where(clubid: ^clubid)
        |> Repo.delete_all
    end
end