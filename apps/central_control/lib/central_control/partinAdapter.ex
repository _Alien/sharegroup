defmodule CentralControl.PartInAdapter do

    alias CentralControl.PartIn
    alias CentralControl.Repo

    require Ecto.Query

    @doc """
    return  {:ok/:error, user/changset}
    """
    def partin(params) do
        %PartIn{} 
        |> PartIn.changeset(params)
        |> Repo.insert
    end

    @doc """
    显示用户参加的所有社团
    return \\[]
    """
    def clublist(userid) do
        PartIn
        |> Ecto.Query.where(userid: ^userid)
        |> Repo.all
    end

    @doc """
    return nil or partin
    """
    def inclub(userid, clubid) do
        PartIn
        |> Ecto.Query.where(userid: ^userid)
        |> Ecto.Query.where(clubid: ^clubid)
        |> Repo.one
    end
    
    @doc """
    return []
    """
    def memberlist(clubid) do
        PartIn
        |> Ecto.Query.where(clubid: ^clubid)
        |> Repo.all
    end

    def quit(userid, clubid) do
        case inclub(userid, clubid) do
            nil ->
                {:error, "no such id"}
            res ->
                Repo.delete(res)
        end
    end

    def deleteclub(clubid) do
        PartIn
        |> Ecto.Query.where(clubid: ^clubid)
        |> Repo.delete_all
    end

end