defmodule CentralControl.ManagerAdapter do
    
    alias CentralControl.Manager
    alias CentralControl.Repo

    require Ecto.Query

    def create_manager(params) do
        %Manager{} |> Manager.changeset(params) |> Repo.insert
    end

    def listManager(clubid) do
        Manager
        |> Ecto.Query.where(clubid: ^clubid)
        |> Repo.all
    end

    def isManager(userid, clubid) do
        Manager
        |> Ecto.Query.where(clubid: ^clubid)
        |> Ecto.Query.where(userid: ^userid)
        |> Repo.one
    end

    def delete_manager(params) do
        clubid = Map.get(params, :clubid)
        userid = Map.get(params, :userid)

        res = Manager
                    |> Ecto.Query.where(userid: ^userid)
                    |> Ecto.Query.where(clubid: ^clubid)
                    |> Repo.one
        case res do
            nil ->
                {:error, "not found!"}
            manager ->
                Repo.delete(manager)
        end
    end

    def deleteclub(clubid) do
        Manager
        |> Ecto.Query.where(clubid: ^clubid)
        |> Repo.delete_all
    end
end