defmodule CentralControlTest do
  use ExUnit.Case
  alias CentralControl.OwnerAdapter
  alias CentralControl.ManagerAdapter
  alias CentralControl.PartInAdapter
  alias CentralControl.Repo

  doctest CentralControl

  test "the truth" do
    assert 1 + 1 == 2
  end

  test "user create partin manager quit and delelte club" do
    craeteParams = %{userid: 1, name: "be a coder", introduction: "coding is funny"}

    {:ok, _} = CentralControl.user_create_club(craeteParams)
    owner = CentralControl.show_own_club(1)
    assert length(owner) == 1
    first = List.first(owner)
    invitecode = CentralControl.show_club_invitecode(first.clubid)
    clubid = first.clubid

    partinParams = %{clubid: clubid, invitecode: invitecode}

    idlist = [2, 3, 4, 5, 6]
    Enum.each(idlist, 
          fn x ->  partinParams
                  |>Map.put(:userid, x)
                  |>CentralControl.user_partin_club
          end)
    
    {:ok, _} = CentralControl.user_manager_club(%{userid: 4, invitecode: invitecode})
    {:ok, _} = CentralControl.user_quit_club(2, clubid)
    {:ok, _} = CentralControl.user_delete_club(%{userid: 1, invitecode: invitecode})

  end
end
