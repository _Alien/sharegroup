defmodule CentralControl.Repo.Migrations.CreateManager do
  use Ecto.Migration

  def change do
    create table(:manager) do
      add :userid, :integer
      add :clubid, :integer

      timestamps()
    end
  end
end
