defmodule CentralControl.Repo.Migrations.CreateOwner do
  use Ecto.Migration

  def change do
    create table(:owner) do
      add :userid, :integer
      add :clubid, :integer

      timestamps()
    end
  end
end
