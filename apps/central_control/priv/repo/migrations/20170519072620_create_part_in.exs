defmodule CentralControl.Repo.Migrations.CreatePartIn do
  use Ecto.Migration

  def change do
    create table(:partin) do
      add :userid, :integer
      add :clubid, :integer
      add :duty, :string, default: "成员"

      timestamps()
    end
  end
end
