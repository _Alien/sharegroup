defmodule MessageServer.Repo.Migrations.CreateMessageContent do
  use Ecto.Migration

  def change do
      create table(:messagecontent) do
        add :content, :string
        
        timestamps()
      end
  end
end
