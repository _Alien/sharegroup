defmodule MessageServer.Repo.Migrations.CreateMessage do
  use Ecto.Migration

  def change do
    create table(:message) do
      add :type, :integer
      add :status, :boolean, default: false # false NOT READ | true read
      add :srcid, :integer
      add :destid, :integer
      add :contentid, :integer

      timestamps()
    end
  end
end
