defmodule MessageServer.MessageContentAdapter do
    alias MessageServer.Repo
    alias MessageServer.MessageContent
    require Ecto.Query

    def create_message_content(params) do
        changeset = %MessageContent{} |> MessageContent.changeset(params)

        case changeset.valid? do
            true ->
                case Repo.insert(changeset) do
                    {:ok, messagecontent} ->
                        messagecontent
                    {:error, _ } ->
                        nil
                end
            false ->
                nil
        end
    end

    def find_message_content(id) when is_integer(id) do
        MessageContent |> Repo.get(id)
    end
end