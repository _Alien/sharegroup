defmodule MessageServer.MessageContent do
    use Ecto.Schema
    require Ecto.Query

    schema "messagecontent" do
        field :content, :string

        timestamps()
    end

    def changeset(content, params \\ %{}) do
        content
        |> Ecto.Changeset.cast(params, [:content])
    end
end