defmodule MessageServer.Message do
    use Ecto.Schema
    require Ecto.Query

    schema "message" do
        field :type, :integer
        field :status, :boolean, default: false
        field :srcid, :integer
        field :destid, :integer
        field :contentid, :integer

        timestamps()
    end

    def changeset(msg, params \\ %{}) do
        msg
        |> Ecto.Changeset.cast(params, [:type, :status, :srcid, :destid, :contentid])
        |> Ecto.Changeset.validate_required([:type, :status, :srcid, :destid, :contentid])

    end

    @doc """
    提取Ecto.Changeset中的报错信息
    """
    def getErrorMsg([], map) do
        map
    end

    def getErrorMsg(msg, map \\ %{}) do
        [{key, value} | next ] = msg
        getErrorMsg(next, Map.put(map, key, elem(value, 0)))
    end
end