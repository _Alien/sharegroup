defmodule MessageServer do
  @moduledoc """
  Documentation for MessageServer.
  """

  @doc """
  Hello world.

  ## Examples

      iex> MessageServer.hello
      :world

  """
  def hello do
    :world
  end
end
