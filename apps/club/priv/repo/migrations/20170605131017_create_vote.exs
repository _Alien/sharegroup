defmodule Club.Repo.Migrations.CreateVote do
  use Ecto.Migration

  def change do
    create table(:vote) do
      add :clubid, :integer, null: false
      add :theme, :string, null: false
      add :type, :integer, default: 0 # 0为单选， 1为多选
      add :num, :integer, default: 1 # 单选时默认只能选一个， 而多选时候可以限制选择的个数
      add :content, :string
      
      timestamps()
    end
  end
end
