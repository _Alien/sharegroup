defmodule Club.Repo.Migrations.CreateClub do
  use Ecto.Migration

  def change do
    create table(:club) do
      add :invitecode , :string, null: false
      add :name, :string, null: false
      add :introduction, :string
      add :icon, :string
      add :school, :string

      timestamps()
    end

    create unique_index(:club, [:invitecode])
  end
end
