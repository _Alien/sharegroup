defmodule Club.Repo.Migrations.CreateActivity do
  use Ecto.Migration

  def change do
    create table(:activity) do
      add :userid, :integer
      add :clubid, :integer
      add :title, :string
      add :starttime, :naive_datetime
      add :endtime, :naive_datetime
      add :place, :string
      add :content, :string
      add :attachmentid, :integer

      timestamps()
    end
  end
end
