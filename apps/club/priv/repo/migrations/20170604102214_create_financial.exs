defmodule Club.Repo.Migrations.CreateFinancial do
  use Ecto.Migration

  def change do
    create table(:financial) do
      add :clubid, :integer, null: false
      add :type, :integer  #type == 1 收入， 含有收入来源 。 type == 2 支出，有活动名以及用途
      add :amount, :float
      add :reportid, :integer
      add :acceptorid, :integer
      add :comment, :string
      add :src, :string
      add :activity, :string
      add :use, :string
      add :status, :integer, default: 0

      timestamps()
    end
  end
end
