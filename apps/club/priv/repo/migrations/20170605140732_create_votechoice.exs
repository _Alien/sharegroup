defmodule Club.Repo.Migrations.CreateVotechoice do
  use Ecto.Migration

  def change do
    create table(:votechoice) do
      add :voteid, :integer, null: false
      add :userid, :integer, null: false
      add :choice, :string

      timestamps()
    end
  end
end
