defmodule Club.Vote do
    use Ecto.Schema
    require Ecto.Query

    schema "vote" do
      field :clubid, :integer, null: false
      field :theme, :string, null: false
      field :type, :integer, default: 0 # 0为单选， 1为多选
      field :num, :integer, default: 1 # 单选时默认只能选一个， 而多选时候可以限制选择的个数
      field :content, :string

      timestamps()
    end

    def changeset(vote, params \\ %{}) do
        vote
        |> Ecto.Changeset.cast(params, [:clubid, :theme, :type, :num, :content])
        |> Ecto.Changeset.validate_required([:clubid, :theme, :type, :num, :content])
    end
end