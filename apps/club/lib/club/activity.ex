defmodule Club.Activity do
    use Ecto.Schema
    require Ecto.Query

    schema "activity" do
      field :userid, :integer
      field :clubid, :integer
      field :title, :string
      field :starttime, :naive_datetime
      field :endtime, :naive_datetime
      field :place, :string
      field :content, :string
      field :attachmentid, :integer

      timestamps()
    end

    def changeset(activity, params \\ %{}) do
        activity
        |> Ecto.Changeset.cast(params, [:userid, :clubid, :title, :starttime, :endtime, :place, :content, :attachmentid])
        |> Ecto.Changeset.validate_required([:userid, :clubid, :title, :starttime, :endtime, :place, :content])
    end
end