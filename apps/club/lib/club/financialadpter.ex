defmodule Club.FinancialAdapter do
    
    alias Club.Repo
    alias Club.Financial

    require Ecto.Query
    require Logger

    def create(params) do
        changeset = Financial.changeset(%Financial{}, params)

        case changeset.valid? do
            true ->
                case Repo.insert(changeset) do
                    {:ok, financial} ->
                        financial
                    {:error, msg} ->
                        nil
                end
            false ->
                nil
        end
    end

    def get_info_financial(id) when is_integer(id) do
        Financial
        |> Repo.get(id)
    end

    def get_club_financial(clubid) do
        Financial
        |> Ecto.Query.where(clubid: ^clubid)
        |> Repo.all
    end

    def update_financial(id, params) do
        case get_info_financial(id) do
            nil ->
                {:error, "not found"}
            financial ->
                changeset = Financial.changeset(financial, params)
                Repo.update(changeset)
        end
    end

    def delete_financial(id) do
        case get_info_financial(id) do
            nil ->
                {:error, "not found"}
            financial ->
                Repo.delete(financial)
        end
    end
end