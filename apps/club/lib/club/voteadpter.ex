defmodule Club.VoteAdapter do
    alias Club.Repo
    alias Club.Vote
    alias Club.VoteChoice
    require Ecto.Query
    require Logger

    def create(params) do
        changeset = Vote.changeset(%Vote{}, params)

        case changeset.valid? do
            true ->
                case Repo.insert(changeset) do
                    {:ok, vote} ->
                        vote
                    {:error, msg} ->
                        nil
                end
            false ->
                nil
        end
    end

    def get_info_vote(id) do
        Vote
        |> Repo.get(id)
    end

    def get_club_vote(clubid) do
        Vote
        |> Ecto.Query.where(clubid: ^clubid)
        |> Repo.all
    end

    def delete_vote(id) do
        case get_info_vote(id) do
            nil ->
                {:error, "not found"}
            vote ->
                Repo.delete(vote)
        end
    end

    def create_vote_choice(params) do
        changeset = VoteChoice.changeset(%VoteChoice{}, params)

        case changeset.valid? do
            true ->
                case Repo.insert(changeset) do
                    {:ok, choice} ->
                        choice
                    {:error, msg} ->
                        nil
                end
            false ->
                nil
        end
    end

    def has_choice?(userid, voteid) do
        VoteChoice
        |> Ecto.Query.where(voteid: ^voteid)
        |> Ecto.Query.where(userid: ^userid)
        |> Repo.one
    end

    def get_user_choice(id) do
        VoteChoice
        |> Repo.get(id)
    end

    def get_vote_choice_list(id) do
        VoteChoice
        |> Ecto.Query.where(voteid: ^id)
        |> Repo.all
    end

    def count_vote(voteid) do
        case get_info_vote(voteid) do
            nil ->
                {:error, "not exist"}
            vote ->
                res = vote.id
                        |> get_vote_choice_list
                        |> count_every_note
                {:ok, content} = Poison.decode(vote.content)
                {:ok, %{result: res, choice: content} }
        end
    end

    defp count_every_note(list) do
        count_every_note(list, %{})
    end

    defp count_every_note([], count) do
        count
    end

    defp count_every_note(list, count) do
        [head | next] = list
        {:ok, choice} = Poison.decode(head.choice)
        IO.inspect choice
        count = count_choice(choice, count)
        count_every_note(next, count)
    end

    defp count_choice([], count) do
        count    
    end

    defp count_choice(choice, count) do
        [head | next] = choice
        head = Integer.to_string(head)
        count = case Map.get(count, head) do
                    nil ->
                        Map.put(count, head, 1)
                    value ->
                        Map.put(count, head, value + 1)
                    end
        count_choice(next, count)
    end
end