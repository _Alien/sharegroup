defmodule Club.VoteChoice do
    use Ecto.Schema
    require Ecto.Query

    schema "votechoice" do
      field :voteid, :integer, null: false
      field :userid, :integer, null: false
      field :choice, :string

      timestamps()
    end

    def changeset(choice, params \\ %{}) do
        choice
        |> Ecto.Changeset.cast(params, [:voteid, :userid, :choice])
        |> Ecto.Changeset.validate_required([:voteid, :userid, :choice])
    end
end