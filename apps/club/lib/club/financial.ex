defmodule Club.Financial do
    use Ecto.Schema
    require Ecto.Query

    schema "financial" do
      field :clubid, :integer, null: false
      field :type, :integer  #type == 1 收入， 含有收入来源 。 type == 2 支出，有活动名以及用途
      field :amount, :float
      field :reportid, :integer
      field :acceptorid, :integer
      field :comment, :string
      field :src, :string
      field :activity, :string
      field :use, :string
      field :status, :integer, default: 0 # 0 为未处理， 1为接受， 2为拒绝

      timestamps()
    end

    def changeset(financial, params \\ %{}) do
        financial
        |> Ecto.Changeset.cast(params, [:clubid, :type, :amount, :reportid, :acceptorid, :comment, :src, :activity, :use, :status])
        |> Ecto.Changeset.validate_required([:clubid, :type, :amount, :reportid])
    end
end