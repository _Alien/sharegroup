defmodule Club.SchemaClub do
    use Ecto.Schema
    require Ecto.Query

    schema "club" do
      field :invitecode, :string, null: false
      field :name, :string, null: false
      field :introduction, :string
      field :icon, :string
      field :school, :string

      timestamps()
    end

    def changeset(club, params \\ %{}) do
        club
        |> Ecto.Changeset.cast(params, [:invitecode, :name, :introduction, :icon, :school])
        |> Ecto.Changeset.validate_required([:invitecode, :name])
        |> Ecto.Changeset.unique_constraint(:invitecode, name: :club_invitecode_index)
    end

    @doc """
    提取Ecto.Changeset中的报错信息
    """
    def getErrorMsg([], map) do
        map
    end

    def getErrorMsg(msg, map \\ %{}) do
        [{key, value} | next ] = msg
        getErrorMsg(next, Map.put(map, key, elem(value, 0)))
    end
end