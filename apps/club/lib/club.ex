defmodule Club do
  @moduledoc """
  Documentation for Club.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Club.hello
      :world

  """
  def hello do
    :world
  end
end
