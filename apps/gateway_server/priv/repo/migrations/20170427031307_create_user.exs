defmodule ShareGroup.Repo.Migrations.CreateUser do
  use Ecto.Migration

  def change do
    create table(:user) do
      add :telephone, :string, null: false
      add :mail, :string, null: false
      add :username, :string, null: false
      add :password, :string, null: false
      add :timetable, :string

      timestamps()
    end

    create unique_index(:user, [:telephone, :mail])
  end
end
