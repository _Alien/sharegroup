defmodule GatewayServer do
  @moduledoc """
  Documentation for GatewayServer.
  """

  @doc """
  Hello world.

  ## Examples

      iex> GatewayServer.hello
      :world

  """
  def hello do
    :world
  end
end
