defmodule GatewayServer.GuardianSerializer do
    @behaviour Guardian.Serializer

    require Logger

    alias ShareGroup.Repo
    alias ShareGroup.User

    def for_token(user = %User{}), do: {:ok, "User:#{user.id}"}
    def for_token(_), do: { :error, "Unknown resource type" }

    def from_token("User:" <> id) do

        Logger.info "find by id: "<> id
         { :ok, Repo.get_by(User,id: String.to_integer(id)) }
    end
    def from_token(_), do: { :error, "Unknown resource type" }
end