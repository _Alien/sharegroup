defmodule ShareGroup.Email do
    import Bamboo.Email
    

    def welcome_email(keyword) do
        new_email(
            keyword ++ [subject: "欢迎使用社谷网站.", html_body: "<a href=\"http://www.baihe.com/\">http://www.baidu.com/</a>", text_body: "Thanks for joining!"]
        )
    end

    def validate_email(keyword) do
        new_email(
            keyword ++ [subject: "社谷：验证（如果不是你账号请忽略）"]
        )
    end
end