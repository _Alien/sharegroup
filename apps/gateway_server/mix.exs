defmodule GatewayServer.Mixfile do
  use Mix.Project

  def project do
    [app: :gateway_server,
     version: "0.1.0",
     build_path: "../../_build",
     config_path: "../../config/config.exs",
     deps_path: "../../deps",
     lockfile: "../../mix.lock",
     elixir: "~> 1.4",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps()]
  end

  # Configuration for the OTP application
  #
  # Type "mix help compile.app" for more information
  def application do
    # Specify extra applications you'll use from Erlang/Elixir
    [extra_applications: [:logger, :crypto, :bamboo, :bamboo_smtp, :plug],
     mod: {GatewayServer.Application, []}]
  end

  # Dependencies can be Hex packages:
  #
  #   {:my_dep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:my_dep, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
  #
  # To depend on another app inside the umbrella:
  #
  #   {:my_app, in_umbrella: true}
  #
  # Type "mix help deps" for more examples and options
  defp deps do
    [{:cowboy, "~>1.0.0"}, # cowboy 是一个简易使用的http服务器
     {:plug, "~>1.0"}, # 一个处理 连接的模块
     {:poison, "~>3.0.0"}, # JSON 库
     {:ecto, "~>2.0"}, # 一个抽象、封装好了处理数据库请求的库
     {:mariaex, "~>0.8.0"}, # 处理项目和mysql之间的请求操作
     {:jose, "~>1.8"},
     {:poison, "~>3.0"},
     {:uuid, "~>1.1"},
     {:guardian, "~> 0.14.2"},
     {:bamboo_smtp, "~> 1.3.0"},
     {:central_control, in_umbrella: true},
     {:secure_random, "~> 0.5"}
     ]
  end
end
