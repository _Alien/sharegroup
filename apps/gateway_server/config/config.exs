# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
use Mix.Config

config :gateway_server, ShareGroup.Repo,
  adapter: Ecto.Adapters.MySQL,
  database: "gateway_server_repo",
  username: "your_username",
  password: "your_password",
  hostname: "localhost",
  port: 3306

config :gateway_server, ecto_repos: [ShareGroup.Repo]


config :guardian, Guardian,
    allowed_algos: ["HS512"], # optional
    verify_module: Guardian.JWT,  # optional
    issuer: "gateway_server",
    ttl: {30, :days},
    allowed_drift: 2000,
    verify_issuer: true,
    secret_key: %{"k" => "/eOBdinSdsTgDXuVhAdT4MdwgQfVzSk7aE5GtgZloV4=",
                  "kty" => "oct"  
                },
    serializer: GatewayServer.GuardianSerializer

config :gateway_server, ShareGroup.Mailer,
        adapter: Bamboo.SMTPAdapter,
        server: "smtp.163.com",
        port: 465,
        username: "15521283615@163.com",
        password: "1qaz2wsx",
        tls: :never, # can be `:always` or `:never`
        ssl: true, # can be `true`
        retries: 1

# This configuration is loaded before any dependency and is restricted
# to this project. If another project depends on this project, this
# file won't be loaded nor affect the parent project. For this reason,
# if you want to provide default values for your application for
# 3rd-party users, it should be done in your "mix.exs" file.

# You can configure for your application as:
#
#     config :gateway_server, key: :value
#
# And access this configuration in your application as:
#
#     Application.get_env(:gateway_server, :key)
#
# Or configure a 3rd-party app:
#
#     config :logger, level: :info
#

# It is also possible to import configuration files, relative to this
# directory. For example, you can emulate configuration per environment
# by uncommenting the line below and defining dev.exs, test.exs and such.
# Configuration from the imported file will override the ones defined
# here (which is why it is important to import them last).
#
#     import_config "#{Mix.env}.exs"
