defmodule ShareGroup.User.Test do
    use ExUnit.Case, async: false

    require Ecto.Query

    alias ShareGroup.User, as: User

    setup do
        userOne  = %User{}
        userOne = userOne                       #用来查询
        |>Map.put(:telephone, "15875310266")
        |>Map.put(:mail, "123456789@qq.com")
        |>Map.put(:username, "alien")
        |>Map.put(:password, "123456789")

        %{userone: userOne}
    end

    test "CRUD", %{userone: userOne} do
        #find no people

        # assert User.findUser(userOne, :telephone) == nil
        # assert User.findUser(userOne, :mail) == nil
        # assert User.findUser(userOne, :username) == []

        # #insert people
        # hash = User.generateHash(userOne.telephone, userOne.password)
        # userTwo = userOne |> User.changeset(%{password: hash})
        # assert userTwo.valid? == true
        # {res, _ } = ShareGroup.Repo.insert(userTwo)
        # assert res == :ok

        # #find again

        # assert User.findUser(userOne, :telephone) != nil
        # assert User.findUser(userOne, :mail) != nil
        # assert User.findUser(userOne, :username) != []

        # #update people

        # newPassword = "987654321"
        # result = User.findUser(userOne, :telephone)
        # assert result != nil
        # newhash = User.generateHash(userOne.telephone, newPassword)
        # result = User.changeset(result, %{password: newhash})
        # assert result.valid? == true
        # {res, _ } = ShareGroup.Repo.update(result)
        # assert res == :ok

        # #delete people

        # result = User.findUser(userOne, :telephone)
        # assert result != nil

        # {res, _ } = ShareGroup.Repo.delete(result)
        # assert res == :ok
    end

    # test "find not people", %{userone: userOne} do
    #     assert User.findUser(userOne, :telephone) == nil
    #     assert User.findUser(userOne, :mail) == nil
    #     assert User.findUser(userOne, :username) == []

    # end

    # test "insert people", %{userone: userOne} do
    #     hash = User.generateHash(userOne.telephone, userOne.password)
    #     userOne = userOne |> User.changeset(%{password: hash})
    #     assert userOne.valid? == true
    #     {res, _ } = ShareGroup.Repo.insert(userOne)
    #     assert res == :ok
    # end

    # test "find again people" , %{userone: userOne} do
    #     assert User.findUser(userOne, :telephone) != nil
    #     assert User.findUser(userOne, :mail) != nil
    #     assert User.findUser(userOne, :username) != []
    # end

    # test "update people", %{userone: userOne} do
        
    #     newPassword = "987654321"
    #     result = User.findUser(userOne, :telephone)
    #     assert result != nil
    #     newhash = User.generateHash(userOne.telephone, newPassword)
    #     result = User.changeset(result, %{password: newhash})
    #     assert result.valid? == true
    #     {res, _ } = ShareGroup.Repo.update(result)
    #     assert res == :ok

    # end

    # test "delete people", %{userone: userOne} do
        
    #     result = User.findUser(userOne, :telephone)
    #     assert result != nil

    #     {res, _ } = ShareGroup.Repo.delete(result)
    #     assert res == :ok
    # end

end