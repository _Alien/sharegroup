defmodule GatewayRouterTest do
    use ExUnit.Case, async: false
    use Plug.Test

    require Logger

    alias ShareGroup.Repo
    alias ShareGroup.User

    @opts GatewayRouter.init([])

    test "get static file" do
        
        conn = conn(:get, "/src/index.html")

        conn = GatewayRouter.call(conn, @opts)

        assert conn.state == :sent
        assert conn.status == 200

    end

    test "not found static file" do

        conn = conn(:get, "/src/inde.html")

        conn = GatewayRouter.call(conn, @opts)

        # IO.inspect conn

        assert conn.state == :sent
        assert conn.status == 404
        assert conn.resp_body == "NOT FOUND"
    end

    test "test login /browser " do

        conn = conn(:post, "/login", "{\"username\":\"15875310267\",\"password\":\"123546789a\"}")
                    |>put_req_header("content-type", "application/json")

        connTwo = GatewayRouter.call(conn, @opts)
        
        IO.inspect connTwo

        assert connTwo.state == :sent
        assert connTwo.status == 404
        assert connTwo.resp_body == "fail to login"

        userOne  = %User{}
        userOne = userOne                       #用来查询
                    |> User.changeset(%{telephone: "15875310267", mail: "123456780@qq.com", username: "alien666", password: "123456789a"})

        #insert people
        hash = User.generateHash("15875310267", "123456789a")
        userTwo = userOne
                    |> Ecto.Changeset.put_change(:password, hash)

        assert userTwo.valid? == true
        {res, _ } = ShareGroup.Repo.insert(userTwo)
        assert res == :ok

        conn = conn(:post, "/login", "{\"username\":\"15875310267\",\"password\":\"123456789a\"}")
                    |>put_req_header("content-type", "application/json")

        connThree = GatewayRouter.call(conn, @opts)
        
        assert connThree.state == :sent
        assert connThree.status == 200
        assert connThree.resp_body == "login suessfully"

        # IO.inspect Guardian.Plug.current_resource(connThree, "visitor")

        IO.inspect connThree

        mykey = "visitor"
        mycookies = Map.get(connThree.cookies, mykey)

        connFour = conn(:get, "/isUser","{\"username\":\"15875310267\",\"password\":\"123546789a\"}")
                    |>put_req_header("content-type", "application/json")
                    |>put_req_cookie(mykey, mycookies)

        
        # IO.inspect connFour

        connFive = GatewayRouter.call(connFour, @opts)



        assert connFive.state == :sent
        assert connFive.status == 200
        assert connFive.resp_body == "alien666 is user"

        #delete people

        userfour = %User{}

        userfour = userfour |> Map.put(:telephone, "15875310267")
        result = User.findUser(userfour, :telephone)
        assert result != nil
        {res, _ } = ShareGroup.Repo.delete(result)
        assert res == :ok
    end

    test "test register /browser" do

        #register 
        registermsg = %{telephone: "15875310268", mail: "892014226@qq.com",
                        password: "123456789a", username: "geek"}
        {:ok, registerJson} = Poison.encode(registermsg)

        conn = conn(:post, "/register", registerJson)
                    |>put_req_header("content-type", "application/json")

        connTwo = GatewayRouter.call(conn, @opts)

        
        assert connTwo.state == :sent
        assert connTwo.status == 200
        assert connTwo.resp_body == "geek register successfully"


        #register with same telephone and email

        connSix = conn(:post, "/register", registerJson) |> put_req_header("content-type", "application/json")
        connThree = GatewayRouter.call(connSix, @opts)

        assert connThree.state == :sent
        assert connThree.status == 400
        connThree.resp_body |> Poison.decode |> IO.inspect

        #register without telephone
        registermsgTwo = %{mail: "892014226@qq.com",
                        password: "123456789a", username: "geek"}
        {:ok, registerJson} = Poison.encode(registermsgTwo)
        connFour = conn(:post, "/register", registerJson)
                    |>put_req_header("content-type", "application/json")

        connFive = GatewayRouter.call(connFour, @opts)

        assert connFive.state == :sent
        assert connFive.status == 400
        connFive.resp_body |> Poison.decode |> IO.inspect

        #delete register
        user = %User{}
        {:ok, _} =  registermsg
                    |> User.findUser(:telephone)
                    |> Repo.delete
    end

    # test "test forget password " do

    #     #register 
    #     registermsg = %{telephone: "15875310269", mail: "myalien666@gmail.com",
    #                     password: "123456789a", username: "laji"}
    #     {:ok, registerJson} = Poison.encode(registermsg)

    #     conn = conn(:post, "/register", registerJson)
    #                 |>put_req_header("content-type", "application/json")

    #     connTwo = GatewayRouter.call(conn, @opts)

    #     assert connTwo.state == :sent
    #     assert connTwo.status == 200
    #     assert connTwo.resp_body == "laji register successfully"

    #     #forget password

    #     getvalidatecode = %{email: "myalien666@gmail.com"}
    #     {:ok, validateJson} = Poison.encode(getvalidatecode)
    #     conn = conn(:post, "/getValidateCode", validateJson)
    #                 |>put_req_header("content-type", "application/json")
    #     connThree = GatewayRouter.call(conn, @opts)

    #     assert connThree.status == 200

    #     IO.inspect connThree

    #     encrypted = connThree.cookies["vode"]
    #     msg = encrypted |> GatewayRouter.validate_decrypt

    #     Logger.info "change password validate " <> msg

    #     {codeincookies, mail} = String.split_at(msg, 8)

    #     postChangePassword = %{validatecode: codeincookies, newpassword: "213ibhu31"}
    #     {:ok, changeJson} = Poison.encode(postChangePassword)
    #     connFive = conn(:post, "/changepsd", changeJson)
    #                 |>put_req_header("content-type", "application/json")

    #     connFive = recycle_cookies(connFive, connThree)
    #     connFour = GatewayRouter.call(connFive, @opts)

    #     assert connFour.resp_body == "change password successfully"

    #     #delete user
    #     user = %User{}
    #     {:ok, _} =  registermsg
    #                 |> User.findUser(:telephone)
    #                 |> Repo.delete
    # end
end