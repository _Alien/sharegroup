/**
 * Created by AranciaCheng on 2017/4/19.
 */
var path = require('path');
var webpack = require('webpack');
var extractTextPlugin = require('extract-text-webpack-plugin');

const PRODUCTION = process.env.NODE_ENV === 'production';
const DEVELOPMENT = process.env.NODE_ENV === 'development';

var entry = PRODUCTION
    ? {
        angular:[
          'angular',
          'angular-ui-router',
          'angular-ui-router/release/stateEvents',
          'angular-animate'
        ],
        sharegroup:[
          'font-awesome-webpack!./font-awesome.config.js',
          './src/index.js'
        ]
      }
    : {
        sharegroup:[
          './src/index.js',
          'webpack/hot/dev-server',
          'webpack-dev-server/client?http://localhost:8080'
        ]
      };

var plugins = PRODUCTION
    ? [
        new webpack.optimize.UglifyJsPlugin({ // 代码混淆、压缩
          components:false,
          mangle:false,
          compress:{
            // 在UglifyJs删除没有用到的代码时不输出警告
            warnings: false,
            // 删除所有的 `console` 语句
            // 还可以兼容ie浏览器
            drop_console: false,
            // 内嵌定义了但是只用到一次的变量
            collapse_vars: false,
            // 提取出出现多次但是没有定义成变量去引用的静态值
            reduce_vars: false,
          }
        }), // 中间的json不配置会更小
        new webpack.LoaderOptionsPlugin({ // 添加兼容性css浏览器内核前缀
          options:{
            postcss:[require('autoprefixer')({browsers:['last 5 versions']})]
          }
        }),
        new extractTextPlugin('style.css') // 整合所有样式到这个文件中
      ]
    : [new webpack.HotModuleReplacementPlugin()];

const cssIdentifier = PRODUCTION
    ? '[hash:base64:10]'
    : '[path][name]-[local]';

const cssLoader = PRODUCTION
    ? extractTextPlugin.extract({
        fallback:'style-loader',
        use:['style-loader','css-loader?localIdentName='+ cssIdentifier,'postcss-loader']
        // loader:'css-loader?localIdentName='+cssIdentifier
        // loaders:['style-loader','css-loader?localIdentName='+ cssIdentifier,'postcss-loader']
      })
    : ['style-loader','css-loader?localIdentName='+ cssIdentifier,'postcss-loader'];
const lessLoader = PRODUCTION
    ? extractTextPlugin.extract({
        fallback:'style-loader',
        use:['css-loader','postcss-loader','less-loader']
      })
    : ['style-loader','css-loader?localIdentName='+ cssIdentifier,'postcss-loader','less-loader'];

module.exports={
  devtool:'source-map',
  entry:entry,
  plugins:plugins,
  module:{
    // 最新的版本中已经改为用use，而不是用loaders的写法
    loaders:[{
      test:   /\.js$/,
      loaders:['babel-loader'],
      exclude:'/node_modules/'
    },{
      test:   /\.(png|jpg|gif)$/,
      loaders:['url-loader?limit=8000&name=images/[hash:12].[ext]'],
      exclude:'/node_modules/'
    },{
      test:   /\.css$/,
      // loaders:['style-loader','css-loader?localIdentName='+ cssIdentifier,'postcss-loader'],
      loaders:cssLoader,
      exclude:'/node_modules/'
    },{
      test:   /\.less$/,
      loaders:lessLoader,
      exclude:'/node_modules/'
    },{
      test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
      loader: "url-loader?limit=10000&mimetype=application/font-woff"
    }, {
      test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
      loader: "file-loader"
    }]
  },
  output:{
    path:path.join(__dirname,'dist'),
    publicPath:'/dist/',
    filename:'[name].bundle.js',
    chunkFilename: '[name]-chunk-[hash:8].js'
  }
}