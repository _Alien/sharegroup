
# ShareGroup

## elixir服务器配置
### 初始化
1. 本地需要搭建好mysql(maria db)数据库
2. 使用root用户进入mysql并添加新的用户

```

mysql> create user 'your_username'@'localhost' identified by 'your_password';

mysql> grant all on *.* to 'your_username'@'localhost' identified by 'your_password';

```


3. 如果做数据库建立以及表的建立。 在命令行界面中切换到当前工作根目录的路径，在确保config/config.exs 文件配置正确的前提下，输入以下命令:

    ```
    mix deps.get        #这一步只是获取依赖，如果只在项目初次构建的时候需要
    ```

    ```
    mix ecto.create
    ```
    
    如果数据库创建成功的话，它将返回:

    ```
    The database for PlugTest.Repo has been created.
    ```

    现在开始做一个数据库迁移了，输入下面命令:

    ```
    mix ecto.migrate
    ```

    如果发现在建立迁移的时候有任何错误，可以使用 `mix ecto.rollback` 将刚才迁移的操作撤销



## angular-1前端配置
前端开发部分依赖npm包管理工具，需要提前配置node环境。

### 初次运行

1. 安装依赖，用于后面进行的编译或开发。

    ```
    npm install --dev
    ```

2. 编译、打包

    ```
    npm run build
    ```

    会编译对应的`*.less`文件，添加浏览器兼容性前缀，并对`*.js`和样式分别打包为`sharegroup.bundle.js`和`style.css`，存放于`/dist`下。修改前端后，需重新运行命令，进行编译和打包。
    

> 如果上面的命令运行出问题，请配置`NODE_ENV`环境变量

## 运行项目

在工作目录下运行命令 `mix run --no-halt`，访问*localhost:7895/src/index.html*即可进入登录页面。


