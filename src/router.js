/**
 * Created by AranciaCheng on 2017/5/5.
 */
// Core Component
import './component/sgApp';
// Services
import './service/principal';
import './service/club_service';
import './service/dynamics_service';

// Components
import './component/login';
import './component/register';
import './component/retrieve';
import './component/sharegroup';

import './component/club';
import './component/club_activity';
import './component/club_activity_dtl';
import './component/club_activity_add';
import './component/club_task';
import './component/club_vote_list';
import './component/club_vote_dtl';
import './component/club_vote_add';
import './component/club_game';
import './component/club_finance';
import './component/club_finance_list';
import './component/club_finance_dtl';
import './component/club_dynamics';
import './component/club_manage';

import './component/valley';

import './component/me';
import './component/founder';
import './component/join';
import './component/dashboard';
import './component/demission';
import './component/boom';

import './component/other';

let states = [
  {
    name: 'sgApp',
    component: 'sgApp',
    abstract: true
  }, {
    parent: 'sgApp',
    name: 'login',
    url: '/login',
    component: 'login',
    resolve: {},
    redirectTo: (trans) => {
      let svc = trans.injector().get('principal');
      return svc.async().then(() => 'sharegroup', () => {
      })
    }
  }, {
    parent: 'sgApp',
    name: 'register',
    url: '/register',
    component: 'register',
    resolve: {}
  }, {
    parent: 'sgApp',
    name: 'retrieve',
    url: '/retrieve',
    component: 'retrieve',
    resolve: {}
  }, {
    name: 'sharegroup',
    component: 'sharegroup',
    url: '/sharegroup',
    redirectTo: (trans) => {
      return trans.injector().getAsync('dynamics')
        .then(() => {
          let svc = trans.injector().get('club_service');
          return {state: 'club', params: {id: svc.current.id}};
        })
    },
    resolve: {
      clublist: (club_service) =>
        club_service.get_clubs()
          .then(response => {
            club_service.clubs = response.data.clublist || {};
            return club_service.clubs;
          }, () => {
            return {};
          }),
      current: (clublist, club_service) => {
        for (var key in clublist) {
          club_service.current.id = key;
          break;
        }
        return club_service.current;
      },
      userinfo: (current,club_service) =>club_service.userinfo,
      dynamics: (userinfo,dynamics_service) => dynamics_service.dynamics
    },
    data: {
      authRequired: true
    }
  }, {
    parent: 'sharegroup',
    name: 'club',
    url: '/club/{id}',
    redirectTo: 'club.activity',
    component: 'club',
    resolve: {
      info: (club_service, $transition$) => {
        club_service.current.id = $transition$.params().id;
        return club_service
          .get_club_info($transition$.params().id)
          .then(response => {
            for (let key in response.data) club_service.userinfo[key] = response.data[key];
            return club_service.userinfo;
          });
      },
      dynamics: (dynamics_service, $transition$) => {
        return dynamics_service
          .get_dynamics($transition$.params().id)
          .then(response => {
            dynamics_service.dynamics.list = response.data.unreadmsg || [];
            return dynamics_service.dynamics;
          })
      }
    }
  }, {
    parent: 'sharegroup',
    name: 'valley',
    url: '/valley',
    component: 'valley',
    resolve: {}
  }, {
    parent: 'sharegroup',
    name: 'me',
    url: '/me',
    component: 'me',
    redirectTo: 'dashboard',
    resolve: {}
  }, {
    parent: 'me',
    name: 'founder',
    url: '/founder',
    component: 'founder',
    resolve: {}
  }, {
    parent: 'me',
    name: 'join',
    url: '/join',
    component: 'join',
    resolve: {}
  }, {
    parent: 'me',
    name: 'dashboard',
    url: '/dashboard',
    component: 'dashboard',
    resolve: {}
  }, {
    parent: 'me',
    name: 'demission',
    url: '/demission?id',
    component: 'demission',
    resolve: {
      club:($transition$)=>$transition$.params().id,
      clublist:(club_service)=>club_service.clubs
    }
  }, {
    parent: 'me',
    name: 'boom',
    url: '/boom?id',
    component: 'boom',
    resolve: {
      club:(club_service,$transition$)=>club_service.clubs[$transition$.params().id]
    }
  }, {
    name: 'other',
    url: '/other',
    component: 'other',
    resolve: {}
  }, {
    name: 'club.activity',
    url: '/activity',
    component: 'activity',
    resolve: {
      acts: (club_service,$transition$) =>
        club_service
          .get_act($transition$.params().id)
          .then(response=>response.data.activitylist,()=>console.log('get acts failed'))
    }
  }, {
    name: 'club.activity_dtl',
    url: '/activity/{act}',
    component: 'activityDtl',
    resolve: {
      activity:($transition$,club_service)=>
        club_service
          .get_act_dtl($transition$.params().act)
          .then(response=>response.data.activity,()=>console.log('get act dtl failed'))
    }
  }, {
    name: 'club.activity_add',
    url: '/activity/add',
    component: 'activityAdd'
  }, {
    name: 'club.task',
    url: '/task',
    component: 'task',
    resolve: {}
  }, {
    name: 'club.vote',
    url: '/vote',
    redirectTo:'club.vote.list'
  }, {
    name: 'club.vote.list',
    url: '/list',
    component:'voteList',
    resolve: {
      votes:(club_service)=>
        club_service.get_vote().then(response=>response.data.votelist,()=>[])
    }
  }, {
    name: 'club.vote.dtl',
    url: '/dtl/{voteid}',
    component:'voteDtl',
    resolve: {
      vote:(club_service,$transition$)=>
        club_service.get_vote_dtl($transition$.params().voteid*1).then(response=>response.data.detail,
                                                                       ()=>{alert('get vote failed!');return null;}),
      result:(vote,club_service,$transition$)=>{
        if(!vote.haschoice) return undefined;
        return require.ensure([],(require)=>{
          require('chart.js');
          return club_service.get_vote_res($transition$.params().voteid*1).then(response=>response.data,()=>undefined)
        },'chart');
      }
    }
  }, {
    name: 'club.vote.add',
    url: '/add',
    component:'voteAdd'
  }, {
    name: 'club.finance',
    url: '/finance',
    component: 'finance',
    resolve: {}
  }, {
    name: 'club.finance_list',
    url: '/finance_list',
    component: 'financeList',
    resolve: {
      bills:($transition$,club_service)=>
        club_service
          .get_bill($transition$.params().id).then(response=>response.data.financiallist,()=>[])
    }
  }, {
    name: 'club.finance_dtl',
    url: '/finance_dtl/{bill}',
    component: 'financeDtl',
    resolve: {
      billId:($transition$)=>$transition$.params().bill,
      bill:($transition$,club_service)=>
        club_service.get_bill_dtl($transition$.params().bill).then(response=>response.data.detail),
      reporter:(bill,club_service)=>{
        let a = [];
        a.push(bill.reportid);
        return club_service.get_names(a).then(res=>(res.data.namelist)[0].username,()=>`unknown`);
      },
      acceptor:(bill,club_service)=>{
        let a = [];
        if(!bill.acceptorid) return `暂无人审核`;
        a.push(bill.acceptorid);
        return club_service.get_names(a).then(res=>(res.data.namelist)[0].username,()=>`unknown`);
      }
    }
  }, {
    name: 'club.game',
    url: '/game',
    component: 'game',
    resolve: {}
  }, {
    name: 'club.manage',
    url: '/manage',
    component: 'manage',
    resolve: {
      members:(club_service)=>club_service.get_members()
        .then(response=>response.data.memberlist,()=>[])
        .then(idlist=>club_service.get_names(idlist).then(res=>res.data.namelist),()=>[])
    }
  }, {
    name: 'club.dynamics',
    url: '/dynamics/{msgid}/{type}',
    component: 'dynamics',
    resolve: {
      msg: (dynamics_service, $transition$) =>
        dynamics_service.get_dynamics_detail($transition$.params().type * 1,$transition$.params().msgid * 1),
      type:($transition$)=>$transition$.params().type*1,
      srcer:(msg,club_service)=>{
        let a=[];
        a.push(msg.srcid);
        return club_service.get_names(a).then(res=>(res.data.namelist)[0].username,()=>`unknown`)
      },
      dester:(msg,club_service)=>{
        let a=[];
        a.push(msg.destid);
        return club_service.get_names(a).then(res=>(res.data.namelist)[0].username,()=>`unknown`)
      }
    }
  }
];
export default states;

