import app from '../app';
/**
 * 提供与社团相关功能的服务
 * 1. 创建社团
 * 2. 允许xx加入社团
 * 3. 允许xx管理社团
 * 4. 解散社团
 * 5. 请求加入社团
 * 6. 退出社团
 * 7. 显示用户当前加入的社团
 * 8. 获取用户在某社团中的信息
 */
app.factory('club_service', ['$q', '$http', '$state', function ($q, $http, $state) {
  // 操作社团-操作码
  // 基本管理OP
  // 社团信息OP
  // 活动模块OP
  // 财务模块OP
  const OP_FOUNDER = 1, OP_PASS_JOIN = 2, OP_PASS_ADMIN = 3, OP_BOOM = 4,
    OP_JOIN = 5, OP_DEMISSION = 6, OP_CLUBS = 7, OP_CLUBS_INFO = 8,
    OP_NEW_ACT = 11,OP_ACT = 12,OP_ACT_DTL = 13,
    OP_INVOICE = 14,OP_BILL = 15, OP_BILL_DTL =16,OP_BILL_MOD=17,
    OP_NEW_VOTE =18,OP_VOTE_LIST=19,OP_VOTE_DTL=20,OP_VOTE=21,OP_VOTE_RES=22,
    OP_MEMS=23;

  let service = {
    clubs:{},                  // 社团列表
    current:{id:""},           // 当前所在社团
    userinfo:{
      isManager:false,
      isOwner:false,
      partintime:0,
      duty:"",
      name:"",
      introduction:"",
      icon:null,
      school:'unknown'
    }
  };

  service.get_names = (idlist)=>
    $http({
      method:'POST',
      url:'/user',
      data:{
        idlist:idlist
      }
    })

  // 创建社团
  service.founder = (name, icon, tenet, school = 'unknown') =>{
    icon = null;
    return $http({
      method: 'POST',
      url: '/club',
      data: {
        opcode: OP_FOUNDER,
        name: name,
        icon: icon,
        introduction: tenet,
        school: school
      }
    }).then(response=>{
      if (response.data.result) {
        alert('创建成功');
        service.clubs[response.data.invitecode] = name;
        service.current.id = response.data.invitecode;
        $state.go('club', {id: response.data.invitecode}, {reload: false});
      }else {
        return alert(`社团创建失败，原因是: ${response.data.errormsg}`);
      }
    },()=>alert(`error`));
  }

  // 允许加入社团
  service.pass_join = (user=2,club=service.current.id) =>
    $http({
      method: 'POST',
      url: '/club',
      data: {
        opcode: OP_PASS_JOIN,
        opid: user*1,
        invitecode: club
      }
    }).then(response=>{
      if(response.data.result){
        alert('审核成功');
        $state.go('club',{id:club},{reload:false});
      }
    });


  // 允许管理社团
  service.pass_admin = () => {
    let user = '724621702@qq.com',
      club = 'xxxx';
    return $http({
      method: 'POST',
      url: '/club',
      data: {
        opcode: OP_PASS_ADMIN,
        userid: user,
        invitecode: club
      }
    });
  };

  // 解散社团
  service.boom = () => {
    let club = 'xxx';
    return $http({
      method: 'POST',
      url: '/club',
      data: {
        opcode: OP_BOOM,
        invitecode: club
      }
    });
  };

  // 请求加入社团
  service.join = (club) =>
    $http({
      method: 'POST',
      url: '/club',
      data: {
        opcode: OP_JOIN,
        invitecode: club
      }
    }).then(response=>{
      alert('已经发送申请加入请求，待管理员审核');
    });

  // 退出社团
  service.demission = (club=service.current.id) =>
    $http({
      method: 'POST',
      url: '/club',
      data: {
        opcode: OP_DEMISSION,
        invitecode: club
      }
    }).then(response=>{
      console.log(response.data);
      delete service.clubs[club];
      service.current.id = "";
      for(let key in service.clubs){service.current.id = key;break;}
      $state.go('club',{id:service.current.id});
    },()=>console.log(`failed`));

  // 获取用户当前加入的社团列表
  service.get_clubs = () =>
    $http({
      method: 'POST',
      url: '/club',
      data: {
        opcode: OP_CLUBS
      }
    });

  // 获取用户在某社团中信息
  service.get_club_info = (club=service.current.id) =>
    $http({
      method: 'POST',
      url: '/club',
      data: {
        opcode: OP_CLUBS_INFO,
        invitecode: club
      }
    });

  // 获取社团内所有活动列表
  service.get_act = (club=service.current.id)=>
    $http({
      method:'POST',
      url:'/club',
      data:{
        opcode:OP_ACT,
        invitecode:club
      }
    });

  // 获取单个活动具体内容
  service.get_act_dtl = (act,club=service.current.id)=>
    $http({
      method:'POST',
      url:'/club',
      data:{
        opcode:OP_ACT_DTL,
        invitecode:club,
        activityid:act*1
      }
    })

  // 发布新的活动
  service.new_act=(title,starttime,endtime,place,content,attachment=0,club=service.current.id)=>
    $http({
      method:'POST',
      url:'/club',
      data:{
        opcode:OP_NEW_ACT,
        invitecode:club,
        title:title,
        starttime:Math.floor(starttime),
        endtime:Math.floor(endtime),
        place:place,
        content:content,
        attachmentid:attachment
      }
    }).then(response=>{
      $state.go('club.activity_dtl',{id:club,act:response.data.activityid},{reload:false});
    },()=>console.log(`添加活动失败`))

  // 提交对应的报账单
  service.invoice = (type,amount,comment,src,activity,use,club=service.current.id)=>
    $http({
      method:'POST',
      url:'/club',
      data:{
        opcode:OP_INVOICE,
        invitecode:club,
        type:type*1,
        amount:amount*1,
        comment:comment || null,
        src:src || null,
        activity:activity || null,
        use:use || null
      }
    }).then(response=>{
      if(response.data.result) {
        alert(`账单上报成功`);
        $state.go($state.current,{id:club},{reload:true});
      }
    })

  // 获取报账列表
  service.get_bill = (club=service.current.id)=>
    $http({
      method:'POST',
      url:'/club',
      data:{
        opcode:OP_BILL,
        invitecode:club
      }
    })

  // 获取账单具体内容
  service.get_bill_dtl = (bill,club=service.current.id)=>
    $http({
      method:'POST',
      url:'/club',
      data:{
        opcode:OP_BILL_DTL,
        invitecode:club,
        financialid:bill*1
      }
    })

  // 修改账单的状态
  service.bill_mod = (bill,status=0,club=service.current.id)=>
    $http({
      method:'POST',
      url:'/club',
      data:{
        opcode:OP_BILL_MOD,
        invitecode:club,
        financialid:bill*1,
        status:status
      }
    }).then(response=>{
      alert(`修改成功`);
      $state.go($state.current,{id:club,bill:bill},{reload:true});
    })

  // 新增某个投票项
  service.new_vote = (theme,type,num,content,club=service.current.id)=>
    $http({
      method:'POST',
      url:'/club',
      data:{
        opcode:OP_NEW_VOTE,
        invitecode:club,
        theme:theme,
        type:type,
        num:num,
        content:content
      }
    }).then(response=>{$state.go('club.vote',{id:club})},
            ()=>alert('新增投票项失败!'))

  // 获取所有投票项
  service.get_vote = (club=service.current.id)=>
    $http({
      method:'POST',
      url:'/club',
      data:{
        opcode:OP_VOTE_LIST,
        invitecode:club
      }
    })

  // 获取单条投票项具体内容
  service.get_vote_dtl = (id,club=service.current.id)=>
    $http({
      method:'POST',
      url:'/club',
      data:{
        opcode:OP_VOTE_DTL,
        invitecode:club,
        voteid:id
      }
    })

  // 投票
  service.vote = (id,choice,club=service.current.id) =>
    $http({
      method:'POST',
      url:'/club',
      data:{
        opcode:OP_VOTE,
        invitecode:club,
        voteid:id,
        choice:choice
      }
    })

  // 查看投票结果
  service.get_vote_res = (id,club=service.current.id)=>
    $http({
      method:'POST',
      url:'/club',
      data:{
        opcode:OP_VOTE_RES,
        invitecode:club,
        voteid:id
      }
    })

  service.get_members = (club=service.current.id)=>
    $http({
      method:'POST',
      url:'/club',
      data:{
        opcode:OP_MEMS,
        invitecode:club
      }
    })

  return service;
}])