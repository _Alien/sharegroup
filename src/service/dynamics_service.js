import app from '../app';
/**
 * 提供消息动态的服务
 * 1. 获取用户在club团队中的未读消息
 * 2. 获取单条消息的具体内容
 */
app.factory('dynamics_service', ['$http', '$state', function ($http, $state) {
  const OP_DYNAMICS = 9, OP_DETAIL=10;
  let service = {
    dynamics:{
      list:[]    // 动态队列
    },
    dnm_id:null, // 动态的id
    dnm_type:0   // 动态的类型
  };

  service.get_dynamics = (club)=>
    $http({
      method:'POST',
      url:'/club',
      data:{
        opcode:OP_DYNAMICS,
        invitecode:club
      }
    })

  service.get_dynamics_detail = (type,msgid)=>{
    service.dnm_type = type;
    service.dnm_id = msgid;
    return $http({
      method:'POST',
      url:'/club',
      data:{
        opcode:OP_DETAIL,
        type:type,
        msgid:msgid
      }
    }).then(response=>{
      let index;
      for(index=0;index<service.dynamics.list.length;index++){if(service.dynamics.list[index].msgid==msgid) break;}
      service.dynamics.list.splice(index,1);
      return response.data.msg;
    })
  }

  return service;
}])