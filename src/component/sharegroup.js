/**
 * Created by AranciaCheng on 2017/5/9.
 */
import app from '../app';
import '../directive/layout';
import '../directive/head_bar';
import '../directive/foot_bar';

app.component('sharegroup', {
  bindings: {
    clublist:'=',
    current:'=',
    userinfo:'=',
    dynamics:'='
  },
  controller: ($scope) => {

  },
  template: `
  <layout>
    <layout-head>
      <head-bar 
        clubs="$ctrl.clublist" current="$ctrl.current" info="$ctrl.userinfo" dynamics="$ctrl.dynamics"></head-bar>
    </layout-head>
    <layout-body><ui-view></ui-view></layout-body>
    <layout-foot><foot-bar current="$ctrl.current"></foot-bar></layout-foot>
  </layout>
  `
});