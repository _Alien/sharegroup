/**
 * Created by AranciaCheng on 2017/5/18.
 */
import app from '../app';
import style from '../less/component/founder.less';
import  '../service/club_service';
import '../directive/fileread';
import '../directive/trigger_file';

app.component('founder', {
  bindings: {},
  controller: ($scope, $state, club_service) => {
    $scope.founder = club_service.founder;
  },
  replace: true,
  template: `
    <div class="${style.founder}">
      <h1 class="module">创建社团</h1>
      <form name="founder_form" ng-submit="founder(name,icon,tenet,school)" novalidate>
        <div class="form-group">
          <label for="name">社团名</label>
          <input type="text" name="name" ng-model="name" ng-minlength="2" ng-maxlength="20" required>
          <span ng-show="founder_form.name.$error.required" class="danger">*</span>
          <span ng-show="founder_form.name.$error.maxlength" class="warning">名字过长</span>
          <span ng-show="founder_form.name.$error.minlength" class="warning">名字太短</span>
          <span ng-show="founder_form.name.$valid" class="success"><i class="fa fa-check" aria-hidden="true"></i></span>
        </div>
        
        <div class="form-group">
          <label for="tenet">社团宗旨</label>
          <input type="text" name="tenet" ng-model="tenet" ng-minlength="1" ng-maxlength="30" required>
          <span ng-show="founder_form.tenet.$error.required" class="danger">*</span>
          <span ng-show="founder_form.tenet.$error.maxlength" class="warning">言简意赅即可哟!</span>
          <span ng-show="founder_form.tenet.$error.minlength" class="warning">宗旨过短!</span>
          <span ng-show="founder_form.tenet.$valid" class="success"><i class="fa fa-check" aria-hidden="true"></i></span>
        </div>
        
        <div class="form-group">
          <label for="school">学校</label>
          <input type="text" name="school" ng-maxlength="18" ng-model="school">
          <span ng-show="founder_form.school.$error.maxlength" class="warning">学校名字太长</span>
        </div>
        
        <div class="form-group">
          <label for="icon">头像</label>
          <span trigger-file="__founder__icon__" style="cursor: pointer;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span>
          <input type="file" accept="image/*" id="__founder__icon__" name="icon" fileread="icon" ng-model="icon" required>
          <img src="{{icon}}" ng-show="icon" alt="">
          <span ng-show="founder_form.icon.$error.required" class="danger">*</span>
          <span ng-show="icon==null" class="warning">文件大小应2mb内</span>
        </div>
        
        <button type="submit" ng-disabled="founder_form.$invalid">创建</button>
      </form>
    </div>
  `
});