/**
 * Created by AranciaCheng on 2017/5/17.
 */
import app from '../app';
import '../service/club_service';

app.component('voteAdd', {
  controller: ($scope, club_service) => {
    $scope.candidates = [];
    $scope.new_vote = club_service.new_vote;
    $scope.plus = () => $scope.candidates.push(undefined);
    $scope.remove = (index) => $scope.candidates.splice(index, 1);
  },
  template: `
    <form ng-submit="new_vote(theme,type,num,candidates)" name="voteadd_form"> 
      <div class="form-group">
        <label for="theme">题目</label>
        <input type="text" name="theme" ng-model="theme" required>
        <span ng-show="voteadd_form.theme.$error.required" class="danger">*</span>
        <span ng-show="voteadd_form.theme.$error.minlength" class="warning">题目过短</span>
        <span ng-show="voteadd_form.theme.$error.maxlength" class="warning">题目过长</span>
        <span ng-show="voteadd_form.theme.$valid" class="success"><i class="fa fa-check" aria-hidden="true"></i></span>
      </div>
      
      <div class="form-group">
        <label for="type">类型</label>
        <input type="radio" name="type" ng-model="type" value="0" required>单选
        <input type="radio" name="type" ng-model="type" value="1" required>多选
        <span ng-show="voteadd_form.type.$error.required" class="danger">*</span>
        <span ng-show="voteadd_form.type.$valid" class="success"><i class="fa fa-check" aria-hidden="true"></i></span>
      </div>
      
      <div ng-show="type==1" class="form-group">
        <label for="num">最大可选数目</label>
        <input type="number" name="num" ng-model="num" ng-init="num=1" min="1">
        <span ng-show="voteadd_form.num.$error.min" class="warning">至少选一项</span>
        <span ng-show="voteadd_form.num.$error.number" class="warning">格式有误</span>
        <span ng-show="voteadd_form.num.$valid" class="success"><i class="fa fa-check" aria-hidden="true"></i></span>
      </div>
      
      <div ng-if="type==0||type==1">
        <span ng-click="plus()"><i class="fa fa-plus-square"></i></span>
        <p ng-repeat="item in candidates track by $index">
          <input type="text" ng-model="candidates[$index]">
          <span ng-click="remove($index)"><i class="fa fa-times-circle danger"></i></span>
        </p>
      </div>
      
      <div class="btn-group">
        <button type="button" class="tag tag-default" ui-sref="club.vote({id:id})">取消</button>
        <button ng-disabled="voteadd_form.$invalid" type="submit" class="tag tag-primary">提交</button>
      </div>
      
    </form>  
  `
});