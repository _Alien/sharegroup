import app from '../app';
import '../service/club_service';
import style from '../less/component/activity.less';
app.component('financeDtl',{
  bindings:{
    bill:'<',
    billId:'@',
    reporter:'<',
    acceptor:'<'
  },
  controller:($scope,club_service)=>{
    $scope.id = club_service.current.id;
    $scope.bill_mod = club_service.bill_mod;
  },
  replace:true,
  template:`
    <div class="${style.detail}" ng-switch="$ctrl.bill.type">
      <a ui-sref="club.finance_list({id:id})">账单列表</a>
        <span> <i class="fa fa-angle-right" aria-hidden="true"></i> {{$ctrl.billId}}</span>
      <div class="sum">
        <h1 ng-bind="$ctrl.reporter"></h1>
        <span ng-bind="$ctrl.bill.createtime*1000 | date:'yyyy-MM-dd HH:mm:ss':'+0800'"></span>
        <span ng-switch-when="1" class="tag tag-success">收入单</span>
        <span ng-switch-when="2" class="tag tag-primary">支出单</span>
      </div>
      <table class="dtl">
        <tr>
          <td><i class="fa fa-jpy fa-lg base" aria-hidden="true"></i>金额</td>
          <td ng-bind="$ctrl.bill.amount"></td>
        </tr>
        
        <tr ng-switch-when="1">
          <td><i class="fa fa-bullhorn fa-lg base" aria-hidden="true"></i>来源</td>
          <td ng-bind="$ctrl.bill.src"></td>
        </tr>
        <tr ng-switch-when="2">
          <td><i class="fa fa-ravelry fa-lg base" aria-hidden="true"></i>活动</td>
          <td ng-bind="$ctrl.bill.activity"></td>
        </tr>
        <tr ng-switch-when="2">
          <td><i class="fa fa-bullhorn fa-lg base" aria-hidden="true"></i>用途</td>
          <td ng-bind="$ctrl.bill.use"></td>
        </tr>
        
        <tr>
          <td><i class="fa fa-id-card-o fa-lg base" aria-hidden="true"></i>审核人</td>
          <td ng-bind="$ctrl.acceptor"></td>
        </tr>
        <tr>
          <td><i class="fa fa-calendar-check-o fa-lg base" aria-hidden="true"></i>审核时间</td>
          <td ng-bind="$ctrl.bill.updatetime*1000 | date:'yyyy-MM-dd HH:mm':'+0800'"></td>
        </tr>
        
        <tr>
          <td><i class="fa fa-comment fa-lg base" aria-hidden="true"></i>备注</td>
          <td ng-bind="$ctrl.bill.comment"></td>
        </tr>
        
        <tr ng-switch="$ctrl.bill.status">
          <td><i class="fa fa-circle base"></i>状态</td>
          <td ng-switch-when="0" class="default"><i class="fa fa-question-circle-o fa-lg"></i>未处理</td>
          <td ng-switch-when="1" class="success"><i class="fa fa-check-circle-o fa-lg"></i>已审核</td>
          <td ng-switch-when="2" class="danger"><i class="fa fa-ban fa-lg"></i>已拒绝</td>
        </tr>
      </table>
      <span class="hr"></span>
      <div class="btns" ng-if="$ctrl.bill.status==0">
        <button ng-click="bill_mod($ctrl.billId,2)" class="tag tag-primary">拒绝</button>
        <button ng-click="bill_mod($ctrl.billId,1)" class="tag tag-success">通过</button>
      </div>
    </div>
  `
});