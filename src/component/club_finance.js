/**
 * Created by AranciaCheng on 2017/5/17.
 */
import app from '../app';
import '../service/club_service';
import style from '../less/component/activity.less';
app.component('finance', {
  bindings: {},
  controller: ($scope, club_service) => {
    $scope.id = club_service.current.id;
    $scope.isManager = club_service.userinfo.isManager;
    $scope.invoice = club_service.invoice;
  },
  template: `
  <div class="${style.add}">
    <div class="topbar">
      <span class="module">报账单</span>
      <a ng-if="isManager" ui-sref="club.finance_list({id:id})">审账</a>
    </div>
    <form name="invoice_form" ng-submit="invoice(type,amount,comment,src,activity,use)">
      <div class="form-group">
        <label for="amount">金额数目</label>
        <input type="number" name="amount" min="0" ng-model="amount" required>
        <span ng-show="invoice_form.amount.$error.required" class="danger">*</span>
        <span ng-show="invoice_form.amount.$error.min" class="warning">报账金额需大于0</span>
        <span ng-show="invoice_form.amount.$error.number" class="warning">格式有误</span>
        <span ng-show="invoice_form.amount.$valid" class="success"><i class="fa fa-check" aria-hidden="true"></i></span>
      </div>
      
      <div class="form-group">
        <label for="type">金额数目</label>
        <label><input type="radio" name="type" ng-model="type" value="1" required>收入</label>
        <label><input type="radio" name="type" ng-model="type" value="2" required>支出</label>
        <span ng-show="invoice_form.type.$error.required" class="danger">*</span>
      </div>
      
      <div ng-switch="type">
        <div ng-switch-when="1" class="form-group">
          <label for="src">来源</label>
          <input type="text" name="src" ng-model="$parent.src" ng-minlength="2" ng-maxlength="20" required>
          <span ng-show="invoice_form.src.$error.required" class="danger">*</span>
          <span ng-show="invoice_form.src.$error.maxlength" class="warning">过长,应小于20字符</span>
          <span ng-show="invoice_form.src.$error.minlength" class="warning">太短，应大于两个字符</span>
          <span ng-show="invoice_form.src.$valid" class="success"><i class="fa fa-check" aria-hidden="true"></i></span>
        </div>
        
        <div ng-switch-when="2">
          <div class="form-group">        
            <label for="activity">活动名</label>
            <input type="text" name="activity" ng-model="$parent.activity" ng-minlength="2" ng-maxlength="20" required>
            <span ng-show="invoice_form.activity.$error.required" class="danger">*</span>
            <span ng-show="invoice_form.activity.$error.maxlength" class="warning">过长,应小于20字符</span>
            <span ng-show="invoice_form.activity.$error.minlength" class="warning">太短，应大于两个字符</span>
            <span ng-show="invoice_form.activity.$valid" class="success"><i class="fa fa-check" aria-hidden="true"></i></span>
          </div>
          <div class="form-group">        
            <label for="use">用途</label>
            <input type="text" name="use" ng-model="$parent.use" ng-minlength="2" ng-maxlength="20" required>
            <span ng-show="invoice_form.use.$error.required" class="danger">*</span>
            <span ng-show="invoice_form.use.$error.maxlength" class="warning">过长,应小于20字符</span>
            <span ng-show="invoice_form.use.$error.minlength" class="warning">太短，应大于两个字符</span>
            <span ng-show="invoice_form.use.$valid" class="success"><i class="fa fa-check" aria-hidden="true"></i></span>
          </div>
        </div>
      </div>
      
      <div class="form-group">        
        <label for="comment">备注</label>
        <input type="text" name="comment" ng-model="comment">
      </div>
      
      <div class="btn-group">
        <button type="button" class="tag tag-default" ui-sref="club.activity({id:id})">取消</button>
        <button ng-disabled="invoice_form.$invalid" type="submit" class="tag tag-primary">提交</button>
      </div>
      
    </form>
  </div>
  `
});