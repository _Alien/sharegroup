/**
 * Created by AranciaCheng on 2017/5/7.
 */
import app from '../app';
import '../service/principal';

app.component('retrieve', {
  bindings: {},
  controller: function ($state, $scope, $timeout, principal) {
    $scope.sentMsg = null;
    $scope.allowRetrieve = true;
    $scope.retrieve = (pw, rPw, code) => {
      if (pw != rPw) return;
      principal.retrieve(pw, code).then(response => {
        alert(response.data);
        $state.go('login');
      }, response => {
        console.log(`error ${response.data}`);
      });
    }
    $scope.getValidateCode = (email) => {
      $scope.allowRetrieve = false;
      $timeout(()=>$scope.allowRetrieve=true,30*1000);
      principal.getValidateCode(email).then(response => $scope.sentMsg = response.data);
    }
  },
  template: `
    <h1 class="module">找回密码</h1>
    <label>email</label>
    <input type="email" ng-model="email">
    <button ng-click="getValidateCode(email)" ng-disabled="!allowRetrieve">发送验证码</button>
    <div ng-bind="sentMsg"></div>
    
    <form name="resetForm" ng-submit="retrieve(newPw,rNewPw,validateCode)" ng-show="sentMsg">
      <label>新密码</label>
      <input type="password" name="newPw" ng-model="newPw" required>
      <span ng-show="resetForm.newPw.$error.required">*</span>
      <span ng-show="resetForm.newPw.$valid">ok</span>
      <br>
      
      <label>重复新密码</label>
      <input type="password" name="rNewPw" ng-model="rNewPw" required>
      <span ng-show="resetForm.rNewPw.$error.required">*</span>
      <span ng-show="resetForm.rNewPw.$valid">ok</span>
      <br>
      <div ng-show="newPw!=rNewPw && resetForm.newPw.$dirty && resetForm.rNewPw.$dirty">密码不一致</div>
      
      <label>验证码</label>
      <input type="text" name="validateCode" ng-model="validateCode" required>
      <span ng-show="resetForm.validateCode.$error.required">*</span>
      <span ng-show="resetForm.validateCode.$valid">ok</span>
      <br>
      
      
      <div ng-show="resetForm.$error.required">still need to fill the form</div>
      <input type="submit" value="retrieve" ng-disabled="resetForm.$invalid"/>
      
    </form>
    
    <a ui-sref="login">登录</a>
    
  `
});