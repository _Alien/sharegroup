/**
 * Created by AranciaCheng on 2017/5/23.
 */
import app from '../app';
import '../service/club_service';
import '../directive/popup';

app.component('demission',{
  bindings:{
    club:'<',
    clublist:'='
  },
  controller:($scope,club_service)=>{
    $scope.demission = club_service.demission;
    $scope.title = '离开团队';
    $scope.msg = 'are you sure to leave';

  },
  template:`
    <h4>demission component</h4>
    <form ng-init="choice=$ctrl.club">
      <div ng-repeat="(key,value) in $ctrl.clublist" ng-class="['form-group',{'tag':choice==key}]"> 
        <label style="width: 100%">
          <span ng-bind="value"></span>
          <input type="radio" ng-model="$parent.choice" value="{{key}}" style="display: none;">
        </label>
      </div>
      <button type="submit" ng-click="flag=true" class="tag">离开团队</button>
    </form>
    <popup title="{{title}}" container="{{msg+choice}}" flag="flag" ng-init="flag=false">
      <btns>
        <button class="tag tag-default">取消</button>
        <button class="tag tag-primary">确定</button>
      </btns>
    </popup>
  `
})