/**
 * Created by AranciaCheng on 2017/5/6.
 */
import app from '../app';
import '../service/principal';

app.component('login', {
  bindings: {},
  controller: function ($scope, $state, principal) {
    $scope.login = (user,password)=>{
      principal.login(user,password).then(response=>{
        if(response.data) {
          principal.setAuth(true);
          $state.go('sharegroup');
        }
      },response=>{
        alert('登录失败:'+response.data);
      });
    }
  },
  template: `
    <form ng-submit="login(username,password)" name="login_form">
      <div class="form-group">
        <label for="username">username</label>
        <input type="text" name="username" ng-model="username" placeholder="手机或邮箱" required><br>
        <span ng-show="login_form.username.$error.required" class="danger">*</span>
      </div>
      
      <div class="form-group">
        <label for="password">pw</label>
        <input type="password" name="password" ng-model="password" required>
        
      </div>
      
      <button type="submit" ng-disabled="login_form.$invalid">LOGIN</button>
    </form>
    
    <a ui-sref="register">注册</a><span> / </span><a ui-sref="retrieve">忘记密码</a>
  `
})