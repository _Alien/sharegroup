/**
 * Created by AranciaCheng on 2017/5/21.
 */
import app from '../app';
import '../service/club_service';

app.component('join',{
  bindings:{},
  controller:($scope,club_service)=>{
    $scope.join = club_service.join;
  },
  template:`
    <h1 class="module">加入社团</h1>
    <form name="join_form" ng-submit="join(invitecode)">
      <div class="form-group">
        <label for="invitecode">邀请码</label>
        <input type="text" name="invitecode" ng-model="invitecode" required>
      </div>
      <button type="submit" ng-disabled="join_form.$invalid">加入社团</button>
    </form>
  `
});