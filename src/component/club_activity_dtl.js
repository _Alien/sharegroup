import app from '../app';
import '../service/club_service';
import style from '../less/component/activity.less';
app.component('activityDtl',{
  bindings:{
    activity:'<'
  },
  controller:($scope,club_service)=>{
    $scope.id = club_service.current.id;
  },
  replace:true,
  template:`
    <div class="${style.detail}">
      <a ui-sref="club.activity({id:id})">所有活动</a>
        <span> <i class="fa fa-angle-right" aria-hidden="true"></i> {{$ctrl.activity.id}}</span>
      <div class="sum">
        <h1 ng-bind="$ctrl.activity.title"></h1>
        <span ng-bind="$ctrl.activity.createtime*1000 | date:'yyyy-MM-dd HH:mm:ss':'+0800'"></span>
      </div>
      <div class="dtl">
        <p>
          <i class="fa fa-bullhorn fa-lg base" aria-hidden="true"></i>
          <span ng-bind="$ctrl.activity.cotent"></span>
        </p>
        <p>
          <i class="fa fa-map-marker fa-lg base" aria-hidden="true"></i>
          <span ng-bind="$ctrl.activity.place"></span>
        </p>
        <p>
          <i class="fa fa-hourglass-start fa-lg base" aria-hidden="true"></i>
          <span ng-bind="$ctrl.activity.starttime*1000 | date:'yyyy-MM-dd HH:mm':'+0800'"></span>  
        </p>
        <p>
          <i class="fa fa-hourglass-end fa-lg base" aria-hidden="true"></i>
          <span ng-bind="$ctrl.activity.endtime*1000 | date:'yyyy-MM-dd HH:mm':'+0800'"></span>
        </p>
        <p>
          <i class="fa fa-file fa-lg base" aria-hidden="true"></i>
          <span ng-if="!$ctrl.activity.attactmentid">暂无附件</span>
          <a ng-repeat="item in $ctrl.activity.attactmentid">{{item}}</a>
        </p>
      </div>
    </div>
  `
});