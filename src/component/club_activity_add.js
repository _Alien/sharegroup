import app from '../app';
import style from '../less/component/activity.less';
import '../service/club_service';
import '../directive/mul_fileread';

app.component('activityAdd',{
  bindings:{

  },
  controller:($scope,club_service)=>{
    $scope.id = club_service.current.id;
    $scope.new_act = club_service.new_act;
  },
  replace:true,
  template:`
  <div class="${style.add}">
    <div class="topbar">
      <span ui-sref="club.activity({id:id})"><i class="fa fa-arrow-left fa-lg pointer"></i></span>
      <span class="module">新增活动发布</span>  
    </div>
    <form name="actadd_form"
      ng-submit="new_act(title,starttime.getTime()/1000,endtime.getTime()/1000,place,content)">
      <div class="form-group">
        <label for="title">活动标题</label>
        <input type="text" name="title" ng-model="title" ng-minlength="2" ng-maxlength="40" required>
        <span ng-show="actadd_form.title.$error.required" class="danger">*</span>
        <span ng-show="actadd_form.title.$error.maxlength" class="warning">名字过长</span>
        <span ng-show="actadd_form.title.$error.minlength" class="warning">名字太短</span>
        <span ng-show="actadd_form.title.$valid" class="success"><i class="fa fa-check" aria-hidden="true"></i></span>
      </div>
      
      <div class="form-group">
        <label for="starttime">开始时间</label>
        <input type="datetime-local" name="starttime" ng-model="starttime" placeholder="MM-ddTHH:mm" min="2017-01-01T00:00:00"  required>
        <span ng-show="actadd_form.starttime.$error.required" class="danger">*</span>
        <span ng-show="actadd_form.starttime.$error.datetimelocal" class="warning"><i class="fa fa-warning"></i></span>
        <span ng-show="actadd_form.starttime.$error.min" class="warning">任时光匆匆流去</span>
        <span ng-show="actadd_form.starttime.$valid" class="success"><i class="fa fa-check" aria-hidden="true"></i></span>
      </div>
      
      <div class="form-group">
        <label for="endtime">结束时间</label>
        <input type="datetime-local" name="endtime" ng-model="endtime" placeholder="MM-ddTHH:mm" min="2017-01-01T00:00:00" required>
        <span ng-show="actadd_form.endtime.$error.required" class="danger">*</span>
        <span ng-show="actadd_form.endtime.$error.datetimelocal" class="warning"><i class="fa fa-warning"></i></span>
        <span ng-show="actadd_form.endtime.$error.min" class="warning">任时光匆匆流去</span>
        <span ng-show="actadd_form.endtime.$valid" class="success"><i class="fa fa-check" aria-hidden="true"></i></span>
      </div>
      
      <div class="form-group">
        <label for="place">活动地点</label>
        <input type="text" name="place" ng-model="place" ng-minlength="2" ng-maxlength="20" required>
        <span ng-show="actadd_form.place.$error.required" class="danger">*</span>
        <span ng-show="actadd_form.place.$error.maxlength" class="warning">名字过长</span>
        <span ng-show="actadd_form.place.$error.minlength" class="warning">名字太短</span>
        <span ng-show="actadd_form.place.$valid" class="success"><i class="fa fa-check" aria-hidden="true"></i></span>
      </div>
      
      <div class="form-group">
        <label for="content">活动内容</label>
        <textarea type="text" name="content" ng-model="content" ng-minlength="2" ng-maxlength="500" cols="90" rows="10" required></textarea>
        <span ng-show="actadd_form.content.$error.required" class="danger">*</span>
        <span ng-show="actadd_form.content.$error.maxlength" class="warning">名字过长</span>
        <span ng-show="actadd_form.content.$error.minlength" class="warning">名字太短</span>
        <span ng-show="actadd_form.content.$valid" class="success"><i class="fa fa-check" aria-hidden="true"></i></span>
      </div>
      
      <div class="form-group">
        <label for="__activity__attachment__">附件</label>
        <button trigger-file="__activity__attachment__" class="tag tag-primary">浏览</button>
        <input type="file" id="__activity__attachment__" name="attachment" id="__activity__attachment__" mul-fileread="attachment" multiple="multiple">
        <span ng-show="actadd_form.attachment.$valid" class="success"><i class="fa fa-check" aria-hidden="true"></i></span>
        <div ng-repeat="file in attachment">
          <span ng-bind="file.name"></span>
          <span ng-bind="file.type"></span>
          <span ng-bind="file.size"></span>
        </div>
      </div>
      <div class="btn-group">
        <button type="button" class="tag tag-default" ui-sref="club.activity({id:id})">取消</button>
        <button ng-disabled="actadd_form.$invalid" type="submit" class="tag">发布</button>
      </div>
    </form>
  </div>
  `
});