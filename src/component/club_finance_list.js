import app from '../app';
import style from '../less/component/activity.less';
import '../service/club_service';

app.component('financeList',{
  bindings:{
    bills:'<'
  },
  controller:($scope,club_service)=>{
    $scope.id = club_service.current.id;
  },
  template:`
  <div class="${style.activity}">
    <div class="topbar">
      <span class="module">账单列表</span>
      <a ui-sref="club.finance({id:id})">报账</a>
    </div>
    <div class="list" ng-repeat="item in $ctrl.bills">
      <a ng-bind="item.financialid" ui-sref="club.finance_dtl({id:id,bill:item.financialid})" class="hashtag"></a>
      <span  ng-bind="item.createtime*1000 | date:'yyyy-MM-dd HH:mm:ss':'+0800'" class="default"></span>
    </div>
  </div>
  `
});