/**
 * Created by AranciaCheng on 2017/5/23.
 */
import app from '../app';
import '../service/club_service';
import '../directive/mul_fileread';

app.component('boom',{
  bindings:{
    club:'<'
  },
  controller:($scope,$http)=>{
    $scope.attachment = [];
    $scope.upload = ()=>{
      let fd = new FormData();
      for(let i=0;i<$scope.attachment.length;i++){
        fd.append('attachment[]',$scope.attachment[i]);
      }
      $http.post('/upload',fd,{
        transformRequest:angular.identity,
        headers:{'Content-Type':undefined}
      }).then(response=>console.log(response.data));
    }


  },
  template:`
    <h1 class="module">解散社团</h1>
    <p>
      你确定想要解散 <span ng-bind="$ctrl.club"></span>
    </p>
    <button class="tag tag-default">确定</button>
    <hr>
    <form>
      <button trigger-file="__boom__attachment__" class="tag tag-default">浏览</button>
      <input type="file" id="__boom__attachment__" multiple="multiple" mul-fileread="attachment">
      <div>
        <li ng-repeat="file in attachment">
          <span class="tag" ng-bind="file.name"></span>
          <span ng-bind="file.type"></span>
          <span ng-bind="file.size"></span>
        </li>
      </div>
      <button class="tag tag-primary" ng-click="upload()">提交</button>
    </form>
  `
})