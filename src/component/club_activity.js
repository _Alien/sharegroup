/**
 * Created by AranciaCheng on 2017/5/17.
 */
import app from '../app';
import style from '../less/component/activity.less';
import '../service/club_service';
app.component('activity',{
  bindings:{
    acts:'<'
  },
  controller:($scope,club_service)=>{
    $scope.id = club_service.current.id;
    $scope.isManager = club_service.userinfo.isManager;
  },
  template:`
    <div class="${style.activity}">
      <div class="topbar">
        <span class="module">活动列表</span> 
        <span ui-sref="club.activity_add({id:id})" ng-if="isManager" class="base pointer"><i class="fa fa-plus-square-o fa-lg"></i></span>
      </div>
      <div class="list" ng-repeat="item in $ctrl.acts">
        <a ng-bind="item.title" ui-sref="club.activity_dtl({id:id,act:item.activityid})" class="hashtag"></a>
        <span  ng-bind="item.createtime*1000 | date:'yyyy-MM-dd HH:mm:ss':'+0800'" class="default"></span>
      </div>
    </div>
  `
});