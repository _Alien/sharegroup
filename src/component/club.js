/**
 * Created by AranciaCheng on 2017/5/9.
 */
import app from '../app';

app.component('club',{
  bindings:{
    info:'=',
    dynamics:'='
  },
  template:`<div ui-view/>`
});