import app from '../app';
import '../service/club_service';

app.component('dynamics',{
  bindings:{
    msg:'<',
    type:'<',
    srcer:'<',
    dester:'<'
  },
  controller:($scope,club_service)=>{
    $scope.pass_join = club_service.pass_join;
    $scope.current = club_service.current.id;
  },
  template:`
    <h1 class="module">新消息详情</h1>
    <div ng-switch="$ctrl.type">
      
      <div ng-switch-when="2">
        <h4 ng-bind="$ctrl.msg.content"></h4>
        <span ng-bind="$ctrl.msg.timestamps*1000 | date:'yyyy-MM-dd':'+0800'"></span>
        <span class="default"><i class="fa fa-send" aria-hidden="true"></i>发件人 {{$ctrl.srcer}}</span>
        <span class="default"><i class="fa fa-envelope-o" aria-hidden="true"></i>收件人 {{$ctrl.dester}}</span>
        <div>
          <button class="tag tag-default" ui-sref="club({id:current})">取消</button>
          <button class="tag tag-primary" ng-click="pass_join($ctrl.msg.srcid)">允许</button>
        </div>
      </div>
      
      <div ng-switch-when="1">
        <span><i class="fa fa-user-circle-o"></i></span>
        <div ng-bind="$ctrl.msg.content"></div>
      </div>
      
    </div>
  `
});