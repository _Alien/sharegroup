/**
 * Created by AranciaCheng on 2017/5/22.
 */
import app from '../app';
import style from '../less/component/dashboard.less';
import '../service/principal';


app.component('dashboard',{
  bindings:{

  },
  controller:($scope,principal)=>{
    $scope.logout = principal.logout;
  },
  replace:true,
  template:`
    <div class="${style.dashboard}">
      <li>用户名</li>
      <li>密码</li>
      <li>手机</li>
      <li>邮箱</li>
      <li>课表</li>
      <li>退出社团</li>
      <li>解散社团</li>
      <button class="tag" type="button" ng-click="logout()">logout</button>
    </div>
  `
})