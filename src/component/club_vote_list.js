/**
 * Created by AranciaCheng on 2017/5/17.
 */
import app from '../app';
import style from '../less/component/activity.less';
import '../service/club_service';

app.component('voteList',{
  bindings:{
    votes:'<'
  },
  controller:($scope,club_service)=>{
    $scope.id = club_service.current.id;
    $scope.isManager = club_service.userinfo.isManager;
  },
  template:`
    <div class="${style.activity}">
      <div class="topbar">
        <span class="module">投票列表</span>
        <a ui-sref="club.vote.add({id:id})" ng-if="isManager">新增投票</a>
      </div>
      <div class="list" ng-repeat="item in $ctrl.votes">
        
        <a ng-bind="item.theme" ui-sref="club.vote.dtl({id:id,voteid:item.id})" class="hashtag"></a>
        
        <span>
          <i ng-if="item.type==0" class="tag tag-success">单选</i>
          <i ng-if="item.type==1" class="tag tag-primary">多选</i>
          <span ng-bind="item.createtime*1000 | date:'yyyy-MM-dd HH:mm:ss':'+0800'" class="default"></span>
        </span>
      </div>
    </div>
    
  `
});