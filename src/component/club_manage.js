import app from '../app';
import style from '../less/component/activity.less';

app.component('manage',{
  bindings:{
    members:'<'
  },
  controller:($scope)=>{

  },
  template:`
    <div class="${style.activity}">
      <div class="topbar">
        <span class="module">成员列表</span>
      </div>
      <div class="list" ng-repeat="item in $ctrl.members">
        <a>{{item.username}}</a>
        <span></span>
      </div>
    </div>
  `
});