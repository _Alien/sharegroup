/**
 * Created by AranciaCheng on 2017/5/6.
 */
import app from '../app';
import '../service/principal';

app.component('register', {
  bindings: {},
  controller: function ($scope,principal) {
    $scope.register = principal.register;
  },
  template: `
    <h1 class="module">注册账号</h1>
    
    <form name="registerForm" ng-submit="register(user,password,email,phone)"> 
      <label>用户名</label>
      <input type="text" name="user" ng-model="user" placeholder="可任意" required>
      <span ng-show="registerForm.user.$error.required">*</span>
      <span ng-show="registerForm.user.$valid">ok</span>
      <br>
      
      <label>密码</label>
      <input type="password" name="password" ng-model="password" required>
      <span ng-show="registerForm.password.$error.required">*</span>
      <span ng-show="registerForm.password.$valid">ok</span>
      <br>
      
      <label>重复密码</label>
      <input type="password" name="repeatPassword" ng-model="repeatPassword" required>
      <span ng-show="registerForm.repeatPassword.$error.required">*</span>
      <span ng-show="registerForm.repeatPassword.$valid">ok</span>
      <br>
      <div ng-show="password!=repeatPassword && registerForm.password.$dirty && registerForm.repeatPassword.$dirty">两次密码不一样</div>
      
      <label>邮箱</label>
      <input type="email" name="email" ng-model="email" placeholder="仅支持163、qq、gmail邮箱" required>
      <span ng-show="registerForm.email.$error.required">*</span>
      <span ng-show="registerForm.email.$valid">ok</span>
      <br>
      <div ng-show="registerForm.email.$error.email">邮箱格式错误</div>
      
      <label>手机</label>
      <input type="text" name="phone" ng-model="phone" ng-pattern="/^\\d{11}$/" required>
      <span ng-show="registerForm.phone.$error.required">*</span>
      <span ng-show="registerForm.phone.$valid">ok</span>
      <br>
      <div ng-show="registerForm.phone.$invalid && registerForm.phone.$dirty">手机格式有误</div>
      <button type="submit" ng-disabled="registerForm.$invalid">注册</button>
    </form>

    
    <a ui-sref="login">登录</a>
  `
})