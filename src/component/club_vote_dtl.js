/**
 * Created by AranciaCheng on 2017/5/17.
 */
import app from '../app';
import '../service/club_service';
import style from '../less/component/activity.less';

app.component('voteDtl', {
  bindings: {
    vote: '<',
    result: '<'
  },
  controller: ($scope, $state, club_service) => {
    $scope.id = club_service.current.id;
    $scope.choice = [];
    $scope.isChecked = (id) => $scope.choice.indexOf(id) >= 0;
    $scope.select = ($event, id, type) => {
      switch (type) {
        case 1:
          if ($event.target.checked) $scope.choice.push(id);
          else $scope.choice.splice($scope.choice.indexOf(id), 1);
          break;
        case 0:
          $scope.choice.pop();
          $scope.choice.push(id);
          break;
      }
    }

    $scope.submit = (voteid) => {
      if (!$scope.choice.length) return;
      club_service.vote(voteid, $scope.choice)
        .then(res => {
          if (res.data.result) $state.go($state.current, null, {reload: true});
        }, () => alert('vote failed!'));
    }

    $scope.paint = (heading, data) => {
      let color = ()=>Math.floor(Math.random() * 1000) % 256;
      let bg_colors = [],bd_colors=[] ,datas = [];
      // init array
      (() => {
        for (let i = 0; i < data.choice.length; i++) {
          datas[i] = data.result[i] || 0;
          let c =`${color()},${color()},${color()}`;
          bg_colors.push(`rgba(${c},0.3)`);
          bd_colors.push(`rgba(${c},1)`);
        }
      })();
      new Chart(document.getElementById("__sg__canvas"), {
        type: 'bar',
        data: {
          labels: data.choice,
          datasets: [{
            label: '显示',
            data: datas,
            borderColor:bd_colors,
            backgroundColor:  bg_colors,
            borderWidth: 2
          }],
        },
        options: {
          scales: {yAxes: [{ticks: {beginAtZero: true}}]},
          title: {
            display: true,
            text: `${heading} - 数据统计图`,
            position:'bottom'
          }
        }
      });
      return true;
    }
  },
  template: `
    <div class="${style.detail}">
      <a ui-sref="club.vote.list({id:id})">所有投票</a>
        <span> <i class="fa fa-angle-right" aria-hidden="true"></i> {{$ctrl.vote.id}}</span>
      <div class="sum">
        <h1 ng-bind="$ctrl.vote.theme"></h1>
      </div>
      <div ng-if="$ctrl.vote.type==0 && !$ctrl.vote.haschoice">
        <p ng-repeat="item in $ctrl.vote.content track by $index">
          <input type="radio" name="$ctrl.vote.theme" ng-click="$parent.select($event,$index,0)" ng-checked="$parent.isChecked($index)">{{item}}
        </p>
      </div>
      <div ng-if="$ctrl.vote.type==1 && !$ctrl.vote.haschoice">
        <p ng-repeat="item in $ctrl.vote.content track by $index">
          <input type="checkbox" name="$ctrl.vote.theme" ng-click="$parent.select($event,$index,1)" ng-checked="$parent.isChecked($index)">{{item}}
        </p>
      </div>

      <div style="display: flex;justify-content: center">
        <div ng-if="$ctrl.vote.haschoice">
          <canvas id="__sg__canvas" width="650px" height="650px" ng-init="paint($ctrl.vote.theme,$ctrl.result)"></canvas>
        </div>
      </div>
      <button ng-show="!$ctrl.vote.haschoice" ng-click="submit($ctrl.vote.id)">投票</button>

    </div>
  `
});