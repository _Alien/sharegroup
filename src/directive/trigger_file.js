/**
 * Created by AranciaCheng on 2017/5/22.
 */
/**
 * 使用场景:隐藏input[type=file]通过另一组件触发点击事件
 * demo:<button trigger-file="icon"> <input type="file" id="icon"/>
 */
import app from '../app';
app.directive('triggerFile',()=>{
  return {
    restrict:'A',
    scope:{
      triggerFile:'@'
    },
    link:(scope,element)=>{
      element.bind('click',function () {
        document.querySelector('#'+scope.triggerFile).click();
      })
    }
  }
})