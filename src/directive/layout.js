/**
 * Created by AranciaCheng on 2017/5/9.
 */
import app from '../app';
import style from '../less/directive/layout.less';
app.directive('layout', () => {
  return {
    restrict: 'E',
    transclude: {
      'navigation': 'layoutHead',
      'container': 'layoutBody',
      'footer': '?layoutFoot'
    },
    template: `
      <div class="${style.layout}">
        <div ng-transclude="navigation" class="head"></div>
        <div ng-transclude="container" class="body"></div>
        <div ng-transclude="footer" class="foot">fallback footer</div>
      </div>
     `
  }
});