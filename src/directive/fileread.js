/**
 * Created by AranciaCheng on 2017/5/18.
 */
import app from '../app';
app.directive('fileread', () => {
  return {
    restrict:'A',
    scope: {
      fileread: '='
    },
    link: function (scope, element, attributes) {
      element.bind('change', function (changeEvent) {

        var reader = new FileReader();
        reader.onload = function (loadEvent) { // 文件读取完成时触发
          scope.$apply(function () {
            scope.fileread = loadEvent.target.result
          });
        }
        let file_limit = 2 * 1024 * 1024, file_reg = /\.(jpg|jpeg|png|ico|gif|svg)$/;
        if (changeEvent.target.files[0]
          && file_reg.test(changeEvent.target.files[0].name)
          && changeEvent.target.files[0].size <= file_limit) {
          reader.readAsDataURL(changeEvent.target.files[0]);
        } else {
          scope.fileread = null;
        }

      });
    }
  }
})