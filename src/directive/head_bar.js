/**
 * Created by AranciaCheng on 2017/5/16.
 */
import app from '../app';
import style from '../less/directive/head.less';
import '../service/club_service';

app.directive('headBar', () => {
  return {
    restrict: 'E',
    replace: true,
    scope: {
      clubs: '=',
      current:'=',
      info:'=',
      dynamics:'='
    },
    controller: ($scope) => {

    },
    template: `
      <div ng-init="showFlag=0" style="position: relative">
        <div class="${style.header}">
      
          <span>
            <i ng-class="['fa','fa-2x',{false:'fa-align-justify',true:'fa-list'}[showFlag==1]]"  ng-click="showFlag=showFlag==1?0:1" aria-hidden="true"></i>
          </span>
  
          <span ng-click="showFlag=showFlag==2?0:2">
            <span ng-bind="clubs[current.id]"></span>
            <i ng-class="['fa','fa-1x',{false:'fa-caret-up',true:'fa-caret-down'}[showFlag==2]]" aria-hidden="true"></i>
          </span>

          <div> 
            <span ng-click="showFlag=showFlag==3?0:3" style="position:relative;">
              <i class="fa fa-bell fa-lg"></i>
              <i class="pound" ng-show="dynamics.list.length"></i> 
            </span>
            <span ng-click="showFlag=showFlag==4?0:4"><i class="fa fa-plus fa-lg"></i></span>
          </div>
          
        </div>
        
        <!-- 侧边栏 -->
        <div class="${style.sidebar}" ng-show="showFlag==1">
          <li>
            <div class="top">
              <div class="top-left">  
                <img src="../../public/avatar.jpg" alt="avatar" class="avatar">
                <span ng-bind="info.school"></span>    
              </div>
              <div class="top-mid">
                <div>
                  <i class="fa fa-diamond" aria-hidden="true"></i>
                  <span>17</span>
                  <span ng-if="info.isManager"><i class="fa fa-user-circle-o" aria-hidden="true"></i></span>
                  <span ng-if="info.isOwner"><i class="fa fa-drupal" aria-hidden="true"></i></span>
                </div>
                <div>
                  <i class="fa fa-bank" aria-hidden="true"></i>
                  <span ng-bind="info.duty.split('/')[1]|| '默认部门'"></span>
                  <span class="tag" ng-bind="info.duty.split('/')[0]||'默认职务'"></span>
                </div>
              </div>
              <span class="top-right" ng-click="showFlag=0"><i class="fa fa-times" aria-hidden="true"></i></span>
            </div>
            <h4 ng-bind="info.introduction"></h4>
            <i class="join-time"><span ng-bind="info.partintime*1000 | date:'yyyy-MM-dd':'+0800'"></span>加入</i>
          </li>
          <li  ng-click="showFlag=0" ui-sref="club.activity({id:current.id})" ui-sref-active="active">社团活动</li>
          <li  ng-click="showFlag=0" ui-sref="club.vote({id:current.id})" ui-sref-active="active">简易投票</li>
          <li  ng-click="showFlag=0" ui-sref="club.finance({id:current.id})" ui-sref-active="active">财务报账</li>
          <li  ng-click="showFlag=0" ui-sref="club.task({id:current.id})" ui-sref-active="active">任务安排</li>
          <li  ng-click="showFlag=0" ui-sref="club.game({id:current.id})" ui-sref-active="active">团建游戏</li>
          <li  ng-click="showFlag=0" ui-sref="club.manage({id:current.id})" ui-sref-active="active" ng-show="info.isManager">成员管理</li>
          <li  ng-click="showFlag=0" ui-sref="demission({id:current.id})" ui-sref-active="active">离开社团</li>
          <li  ng-click="showFlag=0" ui-sref="boom({id:current.id})" ui-sref-active="active" ng-show="info.isOwner">解散社团</li>
        </div><!-- end sidebar -->
      
        <!-- 社团切换 -->
        <div class="${style.clubs}" ng-show="showFlag==2" ng-click="showFlag=0">
          <li ng-repeat="(id,name) in clubs" ui-sref="club({id:id})">{{name}}</li>
        </div>
        
        <!-- 消息查看 -->
        <div class="${style.dynamics}" ng-show="showFlag==3" ng-click="showFlag=0">
          <li ng-if="!dynamics.list.length" style="color:#ddd;">暂无更多信息</li>
          <li ng-repeat="item in dynamics.list" ui-sref="club.dynamics({id:current.id,msgid:item.msgid,type:item.type})">
            <span ng-switch="item.type">
              <i ng-switch-when="1" class="fa fa-user-plus" aria-hidden="true"></i>&nbsp;
              <span ng-switch-when="1">申请加入</span>
              <i ng-switch-when="2" class="fa fa-user-circle-o" aria-hidden="true"></i>&nbsp;
              <span ng-switch-when="2">社团管理</span>
            </span>
            <span ng-bind="item.timestamps*1000 | date:'EEE M-dd':'+0800'" class="timestamp"></span>
          </li>
        </div>
        
        <!-- 加入/创建团队 -->
        <div class="${style.plus}" ng-show="showFlag==4" ng-click="showFlag=0">
          <li ui-sref="join" ui-sref-active="active">加入社团</li>
          <li ui-sref="founder" ui-sref-active="active">创建社团</li>
        </div>
      </div><!-- end head_bar -->
    `
  }
});