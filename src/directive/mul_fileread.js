import app from '../app';
app.directive('mulFileread', () => {
  return {
    restrict:'A',
    scope:{
      mulFileread:'='
    },
    link: function (scope, element, attributes) {
      element.bind('change', function (changeEvent) {
        scope.$apply(()=>{
          scope.mulFileread = changeEvent.target.files;
        })

        // scope.mulFileread = [];
        // let file_list = changeEvent.target.files;
        // for(let i=0;i<file_list.length;i++){
        //   scope.mulFileread.push(file_list[i]);
        // }

        // scope.mulFileread = changeEvent.target.files;
      });
    }
  }
})