/**
 * Created by AranciaCheng on 2017/5/16.
 */
import app from '../app';
import style from '../less/directive/footer.less';
import '../service/club_service';

app.directive('footBar',()=>{
  return {
    restrict:'E',
    scope:{
      current:'='
    },
    replace:true,
    controller:($scope)=>{
    },
    template:`
      <div class="${style.footer}">
        <span ui-sref="club({id:current.id})" ui-sref-active="active">
          <i class="fa fa-home fa-3x" aria-hidden="true"></i>
          <label>社团</label>
        </span>
        <span ui-sref="valley" ui-sref-active="active">
          <i class="fa fa-globe fa-3x" aria-hidden="true"></i>
          <label>社谷</label>
        </span>
        <span ui-sref="me" ui-sref-active="active">
          <i class="fa fa-user fa-3x" aria-hidden="true"></i>
          <label>我</label>
        </span>
      </div>
    `
  }
});