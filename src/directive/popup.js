import app from '../app';
app.directive('popup',()=>{
  return {
    restrict:'E',
    scope:{
      title:'@',
      container:'@',
      flag:'='
    },
    transclude:{
      'btns':'?btns'
    },
    replace:true,
    controller:($scope)=>{
    },
    template:`
      <div class="popup" ng-show="flag">
        <div class="popup-content">
          <h3>
            {{title}}
            <span ng-click="flag=false"><i class="fa fa-times" aria-hidden="true"></i></span>
          </h3>
          <p ng-bind="container"></p>
          <div ng-transclude="btns" class="btns">fallback btns</div>
        </div>
      </div>
    `
  }
});