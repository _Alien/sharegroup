/**
 * Created by AranciaCheng on 2017/5/5.
 */
import app from './app';
import states from './router';

app
  .config(function ($stateProvider, $urlServiceProvider,$qProvider) {
    $qProvider.errorOnUnhandledRejections(false);//禁止由于state使用redirectTo出错信息显示
    $urlServiceProvider.rules.otherwise({state: 'sharegroup'}); // 最顶层默认路由
    states.forEach((s) => $stateProvider.state(s));

  })
  .run(function ($rootScope, $http, $state, $transitions, principal) {
    // 登录状态校验
    let promise = principal.async();
    promise
    .then(response=>{
      // console.log(response.data);
      principal.setAuth(true);
      },response=>{
      // console.log(response.data)
      principal.setAuth(false);
      $state.go('login');
    })
    .then(function () {
      // match state which should be authenticated
      $transitions.onStart({
        to: (state) => state.data != null && state.data.authRequired
      }, function (trans) {
        let svc = trans.injector().get('principal');
        if(!svc.getAuth()){
          return trans.router.stateService.target('login');
        }
      });
    });


  }); // end run
